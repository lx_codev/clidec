goog.provide('clidec.common');

goog.scope(function() {
  function stringEndsWith(s, suffix) {
    return s.indexOf(suffix, s.length - suffix.length) !== -1;
  }

  clidec.common.array1Equals = function(a1, a2) {
    if (!a1 || !a2)
      return false;
    if (a1.length != a2.length)
      return false;
    for (var i = 0, l = a1.length; i < l; ++i) {
      if (a1[i] != a2[i]) {
        return false;
      }
    }
    return true;
  };

  clidec.common.merge = function(o1, o2) {
    for ( var attr in o2) {
      if (typeof o2[attr] === "object" && o2[attr] !== null) {
        if (Array.isArray(o2[attr])) {
          o1[attr] = o2[attr].slice(0);
        } else {
          o1[attr] = o1[attr] || {};
          if (typeof o1[attr] != "object") { // use o2 attribute
            o1[attr] = {};
          }
          clidec.common.merge(o1[attr], o2[attr]);
        }
      } else {
        o1[attr] = o2[attr];
      }
    }
  };

  clidec.common.getFileExtension = function(filePath) {
    var a = filePath.split(".");
    if (a.length === 1 // visible file with no extension
        || (a[0] === "" && a.length === 2)) { // hidden file with no extension
      return "";
    }
    return a.pop().toLowerCase();
  };

  clidec.common.parseParameters = function(parameterStr) {
    var parameterKVs = parameterStr.split(';');
    var parameters = {};
    parameters['default'] = '';
    for (var i = 0; i < parameterKVs.length; ++i) {
      var kv = parameterKVs[i].split('=');
      if (kv.length == 1) {
        parameters['default'] = kv[0];
      } else if (kv.length == 2) {
        parameters[kv[0]] = kv[1];
      }
    }
    return parameters;
  };
});