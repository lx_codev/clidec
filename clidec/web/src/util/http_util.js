goog.provide('clidec.http');

goog.scope(function() {

  clidec.http.stream = function($q, url, data) {
    var xhr = new window.XMLHttpRequest();
    xhr.open('post', url, true);
    var deferred = $q.defer();
    var promise = this.openXhr(deferred, xhr);
    xhr.onprogress = function() {
      var response = xhr.responseText;
      if (response) {
        deferred.notify(response);
      }
    };
    xhr.send(angular.toJson(data));
    return promise;
  };

  clidec.http.cors = function($q, url, method, data) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      // Check if the XMLHttpRequest object has a "withCredentials" property.
      // "withCredentials" only exists on XMLHTTPRequest2 objects.
      xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
      // Otherwise, check if XDomainRequest.
      // XDomainRequest only exists in IE, and is IE's way of making CORS
      // requests.
      xhr = new XDomainRequest();
      xhr.open(method, url);
    } else {
      // Otherwise, CORS is not supported by the browser.
      xhr = null;
    }
    var promise = this.openXhr($q.defer(), xhr);
    xhr.withCredentials = false;
    xhr.send(angular.toJson(data));
    return promise;
  };

  clidec.http.openXhr = function(deferred, xhr) {
    var headers = {
      'Accept' : 'application/json, text/plain, */*',
      'Content-Type' : 'application/json;charset=UTF-8',
    };
    angular.forEach(headers, function(value, key) {
      if (angular.isDefined(value)) {
        xhr.setRequestHeader(key, value);
      }
    });

    var done = goog.bind(function(status, response, headers, statusText) {
      status = Math.max(status, 0);
      var callback = ((200 <= status && status < 300) ? deferred.resolve
          : deferred.reject);
      callback(this.parseResp(response, headers));
    }, this);
    xhr.onload = function() {
      done(xhr.status, xhr.response, xhr.getAllResponseHeaders(),
          xhr.statusText || '');
    };
    xhr.onerror = xhr.onabort = function() {
      done(-1, null, null, '');
    };

    var promise = deferred.promise;
    promise.success = function(fn) {
      promise.then(function(response) {
        fn(response);
      });
      return promise;
    };
    promise.error = function(fn) {
      promise.then(null, function(response) {
        fn(response);
      });
      return promise;
    };
    return promise;
  };

  clidec.http.parseResp = function(response, headers) {
    var parsed = {};
    angular.forEach(headers.split('\n'), function(line) {
      i = line.indexOf(':');
      var key = angular.lowercase(line.substr(0, i).trim());
      if (key) {
        var value = line.substr(i + 1).trim();
        parsed[key] = parsed[key] ? parsed[key] + ', ' + value : value;
      }
    });
    var contentType = parsed['Content-Type'];
    if (contentType && (contentType.indexOf('application/json') === 0)) {
      return angular.fromJson(response);
    } else {
      return response;
    }
  };
});
