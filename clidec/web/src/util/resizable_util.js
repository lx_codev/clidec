goog.provide('clidec.resizable');

goog.scope(function() {

  clidec.resizable.monitorResize = function(element, callbackFn) {
    var requestFrame = window.requestAnimationFrame
        || window.mozRequestAnimationFrame
        || window.webkitRequestAnimationFrame || function(fn) {
          return window.setTimeout(fn, 20);
        };
    var cancelFrame = window.cancelAnimationFrame
        || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame
        || window.clearTimeout;
    var resizeListener = function(e) {
      var win = e.target || e.srcElement;
      if (win.resizeRAF) {
        cancelFrame(win.resizeRAF);
      }
      win.resizeRAF = requestFrame(function() {
        var trigger = win.resizeTrigger;
        trigger.resizeListener.call(trigger, e);
      });
    };
    var obj = element.resizeTrigger = document.createElement('object');
    obj.setAttribute('style', 'display: block; position: absolute;'
        + ' top: 0; left: 0; height: 100%; width: 100%;'
        + ' overflow: hidden; pointer-events: none; z-index: -1;');
    obj.onload = function(e) {
      this.contentDocument.defaultView.resizeTrigger = element;
      this.contentDocument.defaultView.addEventListener('resize',
          resizeListener);
    };
    obj.type = 'text/html';
    element.resizeListener = function(e) {
      callbackFn();
    };
    if (getComputedStyle(element).position == 'static') {
      element.style.position = 'relative';
    }
    element.appendChild(obj);
  };

});