'use strict';

goog.provide('clidec');

goog.require('goog.events.KeyCodes');
goog.require('goog.ui.KeyboardShortcutHandler');

goog.require('clidec.ButtonMenuDrct');
goog.require('clidec.CodeEditorDrct');
goog.require('clidec.NestSplitpaneDrct');
goog.require('clidec.ProgressBarDrct');
goog.require('clidec.TabBarDrct');
goog.require('clidec.TreeDrct');
goog.require('clidec.ProjectServ');
goog.require('clidec.WorkspaceServ');
goog.require('clidec.ConsoleCtrl');
goog.require('clidec.EditorCtrl');
goog.require('clidec.FileTreeCtrl');
goog.require('clidec.FooterCtrl');
goog.require('clidec.GitCtrl');
goog.require('clidec.MavenCtrl');
goog.require('clidec.OutlineCtrl');
goog.require('clidec.PositionCtrl');
goog.require('clidec.SearchCtrl');
goog.require('clidec.TomcatCtrl');
goog.require('clidec.WorkspaceCtrl');

var clidecApp = angular.module('Clidec', [ 'ui.router' ]);

clidecApp.directive('ngButtonMenu', [ '$parse', clidec.ButtonMenuDrct ]);
clidecApp.directive('codeEditor', [ '$parse', '$rootScope',
    clidec.CodeEditorDrct ]);
clidecApp.directive('ngDialog', clidec.DialogDrct);
clidecApp.directive('ngNestSplitpane', clidec.NestSplitpaneDrct);
clidecApp.directive('ngProgressBar', clidec.ProgressBarDrct);
clidecApp.directive('ngTabBar', [ '$parse', clidec.TabBarDrct ]);
clidecApp.directive('ngTree', [ '$parse', clidec.TreeDrct ]);

clidecApp.service('WorkspaceService', [ '$http', '$rootScope',
    clidec.WorkspaceServ ]);
clidecApp.service('ProjectService', [ '$http', '$rootScope',
    'WorkspaceService', clidec.ProjectServ ]);

clidecApp.controller('ConsoleCtrl', [ '$scope', '$http', clidec.ConsoleCtrl ]);
clidecApp.controller('EditorCtrl', [ '$scope', '$rootScope', '$http',
    'WorkspaceService', clidec.EditorCtrl ]);
clidecApp.controller('FileTreeCtrl', [ '$scope', '$rootScope', '$http',
    'ProjectService', clidec.FileTreeCtrl ]);
clidecApp.controller('FooterCtrl', [ '$scope', '$rootScope', '$http',
    'ProjectService', clidec.FooterCtrl ]);
clidecApp.controller('GitCtrl', [ '$scope', '$rootScope', '$q', '$http',
    'ProjectService', clidec.GitCtrl ]);
clidecApp.controller('MavenCtrl', [ '$scope', '$rootScope', '$q', '$http',
    'ProjectService', clidec.MavenCtrl ]);
clidecApp.controller('OutlineCtrl', [ '$scope', '$rootScope', '$http',
    'ProjectService', clidec.OutlineCtrl ]);
clidecApp.controller('SearchCtrl', [ '$scope', '$rootScope', '$q', '$http',
    'ProjectService', clidec.SearchCtrl ]);
clidecApp.controller('TomcatCtrl', [ '$scope', '$rootScope', '$http',
    'ProjectService', clidec.TomcatCtrl ]);
clidecApp.controller('WorkspaceCtrl', [ '$scope', '$http', 'WorkspaceService',
    clidec.WorkspaceCtrl ]);
clidecApp.controller('PositionCtrl', [ '$scope', clidec.PositionCtrl ]);

clidecApp.config([ '$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/');
      $stateProvider.state('default', {
        url : '/',
        views : {
          'header' : {
            templateUrl : 'src/view/header.html'
          },
          'workspace@default' : {
            templateUrl : 'src/view/workspace.html',
            controller : 'WorkspaceCtrl'
          },
          'git@default' : {
            templateUrl : 'src/view/git.html',
            controller : 'GitCtrl'
          },
          'maven@default' : {
            templateUrl : 'src/view/maven.html',
            controller : 'MavenCtrl'
          },
          'tomcat@default' : {
            templateUrl : 'src/view/tomcat.html',
            controller : 'TomcatCtrl'
          },
          'search@default' : {
            templateUrl : 'src/view/search.html',
            controller : 'SearchCtrl'
          },
          'left_pane' : {
            templateUrl : 'src/view/left_pane.html'
          },
          'file_tree@default' : {
            templateUrl : 'src/view/file_tree.html',
            controller : 'FileTreeCtrl'
          },
          'center_pane' : {
            templateUrl : 'src/view/editor.html',
            controller : 'EditorCtrl'
          },
          'right_pane' : {
            templateUrl : 'src/view/right_pane.html'
          },
          'outline@default' : {
            templateUrl : 'src/view/outline.html',
            controller : 'OutlineCtrl'
          },
          'bottom_pane' : {
            templateUrl : 'src/view/bottom_pane.html'
          },
          'console@default' : {
            templateUrl : 'src/view/console.html',
            controller : 'ConsoleCtrl'
          },
          'footer' : {
            templateUrl : 'src/view/footer.html',
            controller : 'FooterCtrl'
          },
          'position@default' : {
            templateUrl : 'src/view/position.html',
            controller : 'PositionCtrl'
          }
        }
      });
    } ]);

clidecApp.run(function($rootScope) {
  var CTRL = goog.ui.KeyboardShortcutHandler.Modifiers.CTRL;
  var saveFileShortcut = new goog.ui.KeyboardShortcutHandler(document);
  saveFileShortcut.registerShortcut('CTRL_S', goog.events.KeyCodes.S, CTRL);
  goog.events.listen(saveFileShortcut,
      goog.ui.KeyboardShortcutHandler.EventType.SHORTCUT_TRIGGERED,
      function(e) {
        $rootScope.$broadcast('SAVE_FILE', {});
      });
  var showDiffShortcut = new goog.ui.KeyboardShortcutHandler(document);
  showDiffShortcut.registerShortcut('CTRL_R', goog.events.KeyCodes.R, CTRL);
  goog.events.listen(showDiffShortcut,
      goog.ui.KeyboardShortcutHandler.EventType.SHORTCUT_TRIGGERED,
      function(e) {
        $rootScope.$broadcast('SHOW_DIFF', {});
      });
});