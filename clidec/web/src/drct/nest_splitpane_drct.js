goog.provide('clidec.NestSplitpaneDrct');

goog.require('goog.dom.ViewportSizeMonitor');
goog.require('goog.ui.SplitPane');
goog.require('goog.ui.SplitPane.Orientation');

goog.scope(function() {

  clidec.NestSplitpaneDrct = function() {
    return {
      restrict : 'AC',
      replace : true,
      link : function($scope, $element, attrs) {

        // ====================================================================

        clidec.SplitPane = function(firstComponent, secondComponent,
            orientation, parent, firstContainerSizeAttr, opt_domHelper) {
          goog.base(this, firstComponent, secondComponent, orientation,
              opt_domHelper);
          this.parent_ = parent;

          this.percentage_ = 50; // initially 50%
          if (firstContainerSizeAttr) {
            var sizeStr = firstContainerSizeAttr;
            if (sizeStr.charAt(sizeStr.length - 1) == '%') {
              this.percentage_ = parseInt(
                  sizeStr.substr(0, sizeStr.length - 1), 10);
            } else {
              var firstContainerSize = parseInt(sizeStr, 10);
              this.percentage_ = firstContainerSize * 100 / this.totalSize();
            }
          }
        };
        goog.inherits(clidec.SplitPane, goog.ui.SplitPane);

        clidec.SplitPane.prototype.totalSize = function() {
          return this.isVertical() ? this.parent_.clientHeight
              : this.parent_.clientWidth;
        };

        clidec.SplitPane.prototype.resize = function() {
          var currentSize = new goog.math.Size(this.parent_.clientWidth,
              this.parent_.clientHeight);
          this.setSize(currentSize);
          var firstContainerSize = this.totalSize() * this.percentage_ / 100;
          this.setFirstComponentSize(firstContainerSize);
        };

        clidec.SplitPane.prototype.enterDocument = function() {
          goog.base(this, 'enterDocument');
          this.getHandler().listen(
              this,
              goog.ui.SplitPane.EventType.HANDLE_DRAG_END,
              function(e) {
                this.percentage_ = this.getFirstComponentSize() * 100
                    / this.totalSize();
              });
        };

        // ====================================================================

        setGoogClass = function(element) {
          var newClassName = element.className.replace('ng-splitpane',
              'goog-splitpane');
          goog.dom.setProperties(element, {
            'class' : newClassName
          });
        };

        getElementToDecorate_ = function(rootElement, className) {
          var childElements = goog.dom.getChildren(rootElement);
          for (var i = 0; i < childElements.length; i++) {
            var childElement = goog.asserts.assertElement(childElements[i]);
            if (goog.dom.classlist.contains(childElement, className)) {
              return childElement;
            }
          }
          return null;
        };

        var createSplitPaneTree_ = function(parentElement, parentSplitPane) {
          var splitpaneElement = getElementToDecorate_(parentElement,
              'ng-splitpane');
          if (splitpaneElement == null) {
            // parent splitpane is leaf component
            goog.style.setStyle(parentElement, 'border', '1px solid black');
            return;
          }
          setGoogClass(splitpaneElement);
          var firstContainerElement = getElementToDecorate_(splitpaneElement,
              'ng-splitpane-first-container');
          if (firstContainerElement == null) {
            console.log('failed to find first container!');
            return;
          }
          setGoogClass(firstContainerElement);
          var secondContainerElement = getElementToDecorate_(splitpaneElement,
              'ng-splitpane-second-container');
          if (secondContainerElement == null) {
            console.log('failed to find second container!');
            return;
          }
          setGoogClass(secondContainerElement);
          var splitpaneHandle = getElementToDecorate_(splitpaneElement,
              'ng-splitpane-handle');
          if (splitpaneHandle == null) {
            console.log('failed to find handle!');
            return;
          }
          setGoogClass(splitpaneHandle);

          var lhs = new goog.ui.Component();
          var rhs = new goog.ui.Component();
          var orientation = splitpaneElement.getAttribute('orientation');
          if (!orientation) {
            orientation = goog.ui.SplitPane.Orientation.HORIZONTAL;
          }
          var firstContainerSize = splitpaneElement
              .getAttribute('first-container-size');
          var splitpane = new clidec.SplitPane(lhs, rhs, orientation,
              parentElement, firstContainerSize);
          splitpane.setContinuousResize(true);
          splitpane.setHandleSize(2);
          splitpane.decorate(splitpaneElement);
          splitpane.resize();

          if (parentSplitPane != null) {
            goog.events.listen(parentSplitPane, goog.events.EventType.CHANGE,
                function(e) {
                  splitpane.resize();
                });
          } else {
            var vsm = new goog.dom.ViewportSizeMonitor();
            goog.events.listen(vsm, goog.events.EventType.RESIZE, function(e) {
              splitpane.resize();
            });
          }

          createSplitPaneTree_(firstContainerElement, splitpane);
          createSplitPaneTree_(secondContainerElement, splitpane);
        };

        createSplitPaneTree_($element[0], null, {});
      }
    };
  };
});
