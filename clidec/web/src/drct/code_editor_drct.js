goog.provide('clidec.CodeEditorDrct');

goog.require('clidec.common');
goog.require('goog.dom');

goog.scope(function() {

  clidec.CodeEditorDrct = function($parse, $rootScope) {
    return {
      restrict : 'EAC',
      link : function($scope, $element, attrs) {
        MODES = {
          'c' : 'text/x-csrc',
          'h' : 'text/x-c++src',
          'cc' : 'text/x-c++src',
          'java' : 'text/x-java',
          'html' : 'text/html',
          'css' : 'text/css',
          'js' : 'text/javascript',
          'sh' : 'text/x-sh',
          'xml' : 'application/xml',
          'sql' : 'text/x-sql',
          'mysql' : 'text/x-mysql',
          'txt' : 'text',
        };
        var getLanguageMode = function(filePath) {
          var ext = clidec.common.getFileExtension(filePath);
          if (MODES.hasOwnProperty(ext)) {
            return MODES[ext];
          }
          return 'text';
        };

        LINTERS = [ 'css', 'js', 'json', 'java' ];
        var hasLanguageLinter = function(filePath) {
          var ext = clidec.common.getFileExtension(filePath);
          return LINTERS.indexOf(ext) != -1;
        };

        var indentUnit = 2;
        var keyMap = 'default'; // TODO(lx): fix the IME shortkey conflict
        var isReadOnly = false; // TODO(lx): refactor this for external file
        var defaultConfig = {
          'autoCloseBrackets' : true,
          'gutters' : [ 'CodeMirror-linenumbers', 'CodeMirror-foldgutter',
              'CodeMirror-lint-markers' ],
          'foldGutter' : true,
          'height' : '100%',
          'highlightSelectionMatches' : true,
          'indentUnit' : indentUnit,
          'indentWithTabs' : false,
          'keyMap' : keyMap,
          'lineNumbers' : true,
          'matchBrackets' : true,
          'readOnly' : isReadOnly,
          'scrollbarStyle' : 'overlay',
          'showCursorWhenSelecting' : true,
          'showTrailingSpace' : true,
          'smartIndent' : true,
          'statementIndentUnit' : (2 * indentUnit),
          'styleActiveLine' : true,
          'styleSelectedText' : true,
          'tabSize' : indentUnit,
          'theme' : 'default',
          'undoDepth' : 500,
        };

        var editors = {
          editors : {},
          current : '',
          addEditor : function(filePath, additionalConfig, content) {
            var config = {};
            var mode = getLanguageMode(filePath);
            config['mode'] = mode;
            config['lint'] = hasLanguageLinter(filePath);
            config['lineWrapping'] = mode == 'text'; // for document
            clidec.common.merge(config, defaultConfig);
            clidec.common.merge(config, additionalConfig);

            var anchor = document.createElement('div');
            goog.dom.setProperties(anchor, {
              'class' : 'code-editor'
            });
            goog.style.setElementShown(anchor, false);
            $element[0].appendChild(anchor);
            var editor = new CodeMirror(anchor, config);
            editor.getDoc().setValue(content);
            editor.anchor = anchor;

            // TODO(lx): listen change event to update editor
            editor.on("change", function(editor, change) {
            });
            this.editors[filePath] = editor;
            return editor;
          },
          addDiffViewer : function(filePath, additionalConfig, content,
              originContent) {
            var config = {};
            var mode = getLanguageMode(filePath);
            config['mode'] = mode;
            config['lineWrapping'] = mode == 'text'; // for document
            config['gutters'] = [ 'CodeMirror-linenumbers' ];
            config['value'] = content;
            config['orig'] = originContent;
            config['highlightDifferences'] = true;
            // config['connect'] = 'align';
            config['readOnly'] = true;
            clidec.common.merge(config, defaultConfig);
            clidec.common.merge(config, additionalConfig);

            var anchor = document.createElement('div');
            goog.dom.setProperties(anchor, {
              'class' : 'code-editor'
            });
            goog.style.setElementShown(anchor, false);
            $element[0].appendChild(anchor);
            var editor = CodeMirror.MergeView(anchor, config);
            editor.anchor = anchor;
            editor.refresh = goog.bind(function() {
              this.edit.refresh();
              this.right.orig.refresh();
            }, editor);
            editor.focus = goog.bind(function() {
              this.edit.focus();
            }, editor);
            this.editors[filePath] = editor;
            return editor;
          },
          removeEditor : function(name) {
            var editor = this.editors[name];
            if (editor) {
              $element[0].removeChild(editor.anchor);
              delete this.editors[name];
              this.current = '';
            }
          },
          updateEditor : function(name, content) {
            var editor = this.editors[name];
            if (editor) {
              editor.getDoc().setValue(content);
            }
          },
          showEditor : function(name, isDiffMode) {
            if (name == this.current) {
              return;
            }
            var editor = this.editors[name];
            if (editor) {
              var currentEditor = this.editors[this.current];
              if (currentEditor) {
                goog.style.setElementShown(currentEditor.anchor, false);
              }
              goog.style.setElementShown(editor.anchor, true);
              editor.refresh();
              editor.focus();
              this.current = name;
            }
          },
          getContent : function(name) {
            var editor = this.editors[name];
            if (editor) {
              if (editor.getDoc) {
                return editor.getDoc().getValue();
              } else if (editor.edit && editor.edit.getDoc) {
                return editor.edit.getDoc().getValue();
              }
            }
            return '';
          }
        };
        if (attrs.ngAs) {
          var setter = $parse(attrs.ngAs).assign;
          if (setter) {
            setter($scope, editors);
          }
        }
      }
    };
  };
});