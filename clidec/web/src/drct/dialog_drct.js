goog.provide('clidec.DialogDrct');

goog.require('goog.ui.Dialog');
goog.require('goog.positioning.Corner');

goog.scope(function() {

  clidec.DialogDrct = function() {
    return {
      restrict : 'AC',
      replace : true,
      scope : {
        name : '=name',
        onOk : '&',
        onCancel : '&'
      },
      link : function($scope, $element, attrs) {
        var anchor = $element[0];
        // closure diglog only work for html body
        document.body.appendChild(anchor);
        $scope.$on('$destroy', function() {
          anchor.parent && anchor.parent.removeChild(anchor);
        });
        // move anchor element's content to dialog
        var contents = goog.array.clone(anchor.childNodes);
        goog.dom.removeChildren(anchor);
        var dialog = new goog.ui.Dialog();
        dialog.decorate(anchor);
        goog.dom.append(dialog.getContentElement(), contents);

        dialog.setTitle(attrs.title || null); // avoid undefined
        dialog.setHasTitleCloseButton(true);
        dialog.setVisible(false);
        // TODO(lx): move focus to input.

        // register function according the name of dialog
        $scope.name = {
          open : function() {
            dialog.setVisible(true);
          },
          close : function() {
            dialog.setVisible(false);
          }
        };

        goog.events.listen(dialog, goog.ui.Dialog.EventType.SELECT,
            function(e) {
              if (e.key == 'ok') {
                $scope.onOk();
              } else if (e.key == 'cancel') {
                $scope.onCancel();
              }
            });
      }
    };
  };

});
