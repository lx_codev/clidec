goog.provide('clidec.ProgressBarDrct');

goog.require('goog.ui.ProgressBar');

goog.scope(function() {

  clidec.ProgressBarDrct = function() {
    return {
      restrict : 'AC',
      replace : true,
      scope : {
        name : '&name',
        progress : '&progress',
      },
      link : function($scope, $element, attrs) {
        var progressBar = new goog.ui.ProgressBar();
        progressBar.decorate($element[0]);
        $scope.$watch($scope.progress, function(progress) {
          progressBar.setValue(progress);
        });
      }
    };
  };

});
