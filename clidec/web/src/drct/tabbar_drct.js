goog.provide('clidec.TabBarDrct');

goog.require('clidec.resizable');
goog.require('goog.positioning.Corner');
goog.require('goog.ui.TabBar');
goog.require('goog.ui.Tab');
goog.require('goog.ui.TabRenderer');
goog.require('goog.ui.registry');

goog.scope(function() {

  clidec.TabBarDrct = function($parse) {
    return {
      restrict : 'AC',
      replace : true,
      link : function($scope, $element, attrs) {

        // ====================================================================

        clidec.Tab = function(content, opt_domHelper) {
          goog.base(this, content, clidec.TabRenderer.getInstance(),
              opt_domHelper);
        };
        goog.inherits(clidec.Tab, goog.ui.Tab);

        clidec.Tab.prototype.performActionInternal = function(e) {
          if (e && e.target
              && goog.dom.classlist.contains(e.target, 'ng-tab-close')) {
            e.stopPropagation();
            var closeEvent = new goog.events.Event(
                goog.ui.Component.EventType.CLOSE, this);
            return this.dispatchEvent(closeEvent);
          } else {
            return goog.base(this, 'performActionInternal', e);
          }
        };

        clidec.Tab.prototype.showCloseButton = function(isVisible) {
          var buttonElement = this.getDomHelper().getElementsByTagNameAndClass(
              'div', 'ng-tab-close', this.getElement())[0];
          goog.style.setElementShown(buttonElement, isVisible);
        };

        // ====================================================================

        clidec.TabRenderer = function() {
          goog.base(this);
        };
        goog.inherits(clidec.TabRenderer, goog.ui.TabRenderer);
        goog.addSingletonGetter(clidec.TabRenderer);

        clidec.TabRenderer.prototype.addCloseButton_ = function(tab, element) {
          var closeButtonElement = tab.getDomHelper().createElement('div');
          goog.dom.classlist.add(closeButtonElement, 'ng-tab-close');
          element.insertBefore(closeButtonElement, element.firstChild);
        };

        clidec.TabRenderer.prototype.createDom = function(tab) {
          element = goog.base(this, 'createDom', tab);
          goog.dom.classlist.add(element, 'ng-closeable-tab');
          this.addCloseButton_(tab, element);
          return element;
        };

        clidec.TabRenderer.prototype.decorate = function(tab, element) {
          goog.dom.classlist.add(element, 'goog-tab');
          element = goog.base(this, 'decorate', tab, element);
          this.addCloseButton_(tab, element);
          return element;
        };

        goog.ui.registry.setDecoratorByClassName('ng-closeable-tab',
            function() {
              return new clidec.Tab('');
            });

        // ====================================================================

        clidec.TabBar = function() {
          goog.base(this);
          this.moreIndicator_ = null;
          this.wrapper_ = null;
          this.moreMenu_ = null;
          this.tabs_ = [];
          this.invisibleTabs_ = [];
        };
        goog.inherits(clidec.TabBar, goog.ui.TabBar);

        clidec.TabBar.prototype.createTab = function() {
          return new clidec.Tab();
        };

        clidec.TabBar.prototype.decorateInternal = function(element) {
          var dom = this.getDomHelper();
          this.moreIndicator_ = dom.createDom('span', {
            'class' : 'ng-tab-bar-more'
          }, '...');
          this.wrapper_ = dom.createDom('div', {
            'class' : 'ng-tab-bar-wrapper'
          }, '', this.moreIndicator_);
          element.parentNode.insertBefore(this.wrapper_, element);
          dom.appendChild(this.wrapper_, element);
          goog.base(this, 'decorateInternal', element);
        };

        clidec.TabBar.prototype.isInFirstRow_ = function(tab) {
          var tabElement = tab.getElement();
          var tabTop = goog.style.getPageOffset(tabElement).y;
          var tabHeight = goog.style.getSize(tabElement).height;
          var tabBarTop = goog.style.getPageOffset(this.wrapper_).y;
          return tabTop - tabHeight <= tabBarTop - 3;
        };

        clidec.TabBar.prototype.addToMore_ = function(tab, index) {
          var menuItem = new goog.ui.MenuItem(tab.getCaption());
          this.moreMenu_.addChildAt(menuItem, index, true);
          goog.events.listen(menuItem, // for auto format
          goog.ui.Component.EventType.ACTION, goog.bind(function(e) {
            var index = -1;
            if ((index = this.invisibleTabs_.indexOf(tab)) !== -1) {
              this.invisibleTabs_.splice(index, 1);
              clidec.TabBar.superClass_.addChildAt.call(this, tab, 0, true);
              this.setSelectedTab(tab);
            } else {
              this.setSelectedTab(tab);
            }
          }, this));
          return menuItem;
        };

        clidec.TabBar.prototype.enterDocument = function() {
          goog.base(this, 'enterDocument');
          // this.attachContextMenu_(this);
          this.moreMenu_ = new goog.ui.PopupMenu();
          for (var i = 0; i < this.getChildCount(); ++i) {
            this.addToMore_(this.getChildAt(i), i);
          }
          this.moreMenu_.render();
          this.moreMenu_.setToggleMode(true);
          this.moreMenu_.attach(this.moreIndicator_,
              goog.positioning.Corner.BOTTOM_RIGHT,
              goog.positioning.Corner.TOP_RIGHT);
          goog.events.listen(this.moreMenu_, // for auto format
          goog.ui.Component.EventType.SHOW, goog.bind(function(e) {
            for (var i = 0; i < this.tabs_.length; ++i) {
              var tab = this.tabs_[i];
              var menuItem = tab.popupMenuItem_;
              if (menuItem && this.moreMenu_.indexOfChild(menuItem) !== -1) {
                var menuItemElement = menuItem.getContentElement();
                if (this.invisibleTabs_.indexOf(tab) !== -1) {
                  goog.style.setStyle(menuItemElement, 'color', 'darkgray');
                } else {
                  goog.style.setStyle(menuItemElement, 'color', 'black');
                }
              }
            }
          }, this));

          // TODO(lx): need to listen wrapper element resize event.
          // goog.events.listen(window, goog.events.EventType.RESIZE,
          // goog.bind(
          // function() {
          // this.updateTabs_();
          // }, this));
          goog.style.setElementShown(this.getElement(), false);
          clidec.resizable.monitorResize(this.wrapper_, goog.bind(
              this.updateTabs_, this));
        };

        clidec.TabBar.prototype.addChildAt = function(tab, index, opt_render) {
          this.tabs_.splice(index, 0, tab);
          goog.base(this, 'addChildAt', tab, index, opt_render);
          var tabElement = tab.getElement();
          var margin = goog.style.getMarginBox(tabElement);
          var tabWidth = goog.style.getBounds(tabElement).width + margin.left
              + margin.right;
          tab.wantedWidth_ = tabWidth;
          if (this.moreMenu_) {
            tab.popupMenuItem_ = this.addToMore_(tab, index);
          }
          this.getHandler().listen(tab, goog.ui.Component.EventType.CLOSE,
              goog.bind(function(selectedTab, e) {
                this.removeChild(selectedTab, true);
                selectedTab.dispose();
                if (this.getChildCount() == 0) {
                  goog.style.setElementShown(this.getElement(), false);
                }
              }, this, tab));
          this.updateTabs_();
          if (!goog.style.isElementShown(this.getElement())) {
            goog.style.setElementShown(this.getElement(), true);
          }
        };

        clidec.TabBar.prototype.removeChild = function(tab, opt_render) {
          var index = -1;
          if ((index = this.tabs_.indexOf(tab)) !== -1) {
            this.tabs_.splice(index, 1);
            if ((index = this.invisibleTabs_.indexOf(tab)) !== -1) {
              this.invisibleTabs_.splice(index, 1);
            } else {
              goog.base(this, 'removeChild', tab, opt_render);
              if (this.moreMenu_) {
                this.moreMenu_.removeChild(tab.popupMenuItem_);
              }
              this.updateTabs_();
            }
          }
        };

        clidec.TabBar.prototype.isThereEnoughSpace_ = function(wantedWidth) {
          var lastTab = this.getChildAt(this.getChildCount() - 1);
          if (!lastTab) {
            return true;
          }
          var lastTabElement = lastTab.getElement();
          var tabRight = goog.style.getPageOffset(lastTabElement).x
              + goog.style.getSize(lastTabElement).width;
          var tabBarRight = goog.style.getPageOffset(this.wrapper_).x
              + goog.style.getSize(this.wrapper_).width;
          // There is small difference between the selected and unselected tab,
          // we add some pixels to avoid flash.
          return tabBarRight - tabRight > wantedWidth + 10;
        };

        clidec.TabBar.prototype.updateTabs_ = function() {
          var tab = this.getChildAt(this.getChildCount() - 1);
          if (tab && this.isInFirstRow_(tab)) {
            while ((tab = this.invisibleTabs_[0])
                && this.isThereEnoughSpace_(tab.wantedWidth_)) {
              this.invisibleTabs_.splice(0, 1);
              clidec.TabBar.superClass_.addChildAt.call(this, tab, this
                  .getChildCount(), true);
            }
          } else {
            while ((tab = this.getChildAt(this.getChildCount() - 1))
                && !this.isInFirstRow_(tab)) {
              this.invisibleTabs_.splice(0, 0, tab);
              clidec.TabBar.superClass_.removeChild.call(this, tab, true);
            }
          }
        };

        // ====================================================================

        adaptClosureComponent = function(component) {
          var element = component.getElement();
          var handlers = {};
          for (var i = 0; i < element.attributes.length; ++i) {
            var attr = element.attributes[i];
            if (attr.name.indexOf('ng-on-') == 0) {
              var eventName = attr.name.slice(6).toLowerCase();
              handlers[eventName] = $parse(attr.value);
              var $scope_ = this;
              goog.events.listen(component, eventName, function(e) {
                // $scope_.$apply(function() { // already in $digest
                handlers[e.type]($scope_, {
                  '$event' : e,
                  '$selected' : $scope_.tabBar.getSelectedTab()
                });
                // });
              });
              // TODO(lx): do we need to consider unlisten the event when
              // component is destroyed.
            }
          }
        };

        setGoogClass = function(element) {
          var newClassName = element.className.replace('ng-tab-bar',
              'goog-tab-bar goog-tab-bar-top');
          goog.dom.setProperties(element, {
            'class' : newClassName
          });
        };

        setGoogClass($element[0]);
        var tabBar = new clidec.TabBar();
        tabBar.decorate($element[0]);
        if (attrs.ngAs) {
          var setter = $parse(attrs.ngAs).assign;
          if (setter) {
            setter($scope, tabBar);
          }
        }
        $scope['tabBar'] = tabBar;
        goog.bind(adaptClosureComponent, $scope)(tabBar);
      }
    };
  };

});
