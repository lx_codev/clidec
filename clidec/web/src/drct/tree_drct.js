goog.provide('clidec.TreeDrct');

goog.require('goog.object');
goog.require('goog.positioning.Corner');
goog.require('goog.ui.tree.TreeControl');
goog.require('goog.ui.tree.TreeNode');
goog.require('goog.ui.PopupMenu');

goog.scope(function() {

  clidec.TreeDrct = function($parse) {
    return {
      restrict : 'AC',
      replace : true,
      link : function($scope, $element, attrs) {

        clidec.TreeControl = function(html, contextMenu, opt_config,
            opt_domHelper) {
          goog.base(this, html, opt_config, opt_domHelper);
          this.contextMenu_ = contextMenu;
        };
        goog.inherits(clidec.TreeControl, goog.ui.tree.TreeControl);

        clidec.TreeControl.prototype.attachContextMenu_ = function(node) {
          var element = node.getElement();
          if (element) {
            this.contextMenu_.attach(element, undefined,
                goog.positioning.Corner.TOP_LEFT, true);
          }
          var children = node.getChildren();
          for (var i = 0; i < children.length; ++i) {
            this.attachContextMenu_(children[i]);
          }
        };

        clidec.TreeControl.prototype.enterDocument = function() {
          goog.base(this, 'enterDocument');
          this.attachContextMenu_(this);
        };

        // /////////////////////////////////////////////////////////////////////

        adaptClosureComponent = function(component) {
          var element = component.getElement();
          for (var i = 0; i < element.attributes.length; ++i) {
            var attr = element.attributes[i];
            if (attr.name.indexOf('ng-on-') == 0) {
              var eventName = attr.name.slice(6).toLowerCase();
              var handler = $parse(attr.value);
              var $scope_ = this;
              goog.events.listen(component, eventName, function(e) {
                $scope_.$apply(function() {
                  handler($scope_, {
                    '$event' : e,
                    '$selected' : $scope_.tree.getSelectedItem()
                  });
                });
              });
              // TODO(lx): do we need to consider unlisten the event when
              // componenta is destroyed.
            }
          }
        };

        adaptClosureComponentRecursively = function(component) {
          goog.bind(adaptClosureComponent, this)(component);
          component.forEachChild(adaptClosureComponentRecursively, this);
        };

        getContextMenuElement_ = function(treeElement) {
          var childElements = goog.dom.getChildren(treeElement);
          for (var i = 0; i < childElements.length; i++) {
            var childElement = goog.asserts.assertElement(childElements[i]);
            if (goog.dom.classlist.contains(childElement, 'goog-menu')) {
              return childElement;
            }
          }
          return null;
        };

        var contextMenu = new goog.ui.PopupMenu();
        var contextMenuElement = getContextMenuElement_($element[0]);
        if (contextMenuElement) {
          contextMenu.decorate(contextMenuElement);
        } else {
          console.log('Failed to find context menu element');
        }
        var treeConfig = goog.ui.tree.TreeControl.defaultConfig;
        var tree = new clidec.TreeControl('', contextMenu, treeConfig);
        tree.setExpanded(false);
        tree.render($element[0]);
        if (attrs.ngAs) {
          var setter = $parse(attrs.ngAs).assign;
          if (setter) {
            setter($scope, tree);
          }
        }
        goog.object.forEach(attrs, function(attrValue, attrName) {
          if (attrName.indexOf('ngOn') == 0) {
            var eventName = attrName.slice(4).toLowerCase();
            var handler = $parse(attrValue);
            goog.events.listen(tree.getElement(), eventName, function(e) {
              $scope.$apply(function() {
                handler($scope, {
                  '$event' : e,
                  '$selected' : tree.getSelectedItem()
                });
              });
            });
          }
        });

        goog.events.listen(contextMenu, goog.ui.Component.EventType.SHOW,
            function(e) {
              for (var i = 0; i < contextMenu.getChildCount(); i++) {
                var menuItem = contextMenu.getChildAt(i);
                var itemElement = menuItem.getElement();
                if (itemElement.hasAttribute('ng-disabled')) {
                  var isDisabled = $scope.$eval(itemElement
                      .getAttribute('ng-disabled'));
                  if (goog.isDef(isDisabled)) {
                    menuItem.setEnabled(!isDisabled);
                  }
                } else if (itemElement.hasAttribute('ng-enabled')) {
                  var isEnabled = $scope.$eval(itemElement
                      .getAttribute('ng-enabled'));
                  if (goog.isDef(isEnabled)) {
                    menuItem.setEnabled(isEnabled);
                  }
                }
              }
            });

        $scope['tree'] = tree;
        goog.bind(adaptClosureComponent, $scope)(tree);
        goog.bind(adaptClosureComponentRecursively, $scope)(contextMenu);

        var createTreeFromData = function(node, data) {
          node.setHtml(data.id);
          var parent = node.getParent();
          node['path'] = (parent ? parent['path'] + attrs.delimitor : '')
              + data.id;
          if (!data.hasOwnProperty('children')) {
            node.setIconClass(treeConfig.cssTreeIcon + ' '
                + treeConfig.cssFileIcon);
            node.setExpandedIconClass(treeConfig.cssTreeIcon + ' '
                + treeConfig.cssFileIcon);
            node['is_leaf'] = true;
          } else {
            node.setIconClass(treeConfig.cssTreeIcon + ' '
                + treeConfig.cssCollapsedFolderIcon);
            node.setExpandedIconClass(treeConfig.cssTreeIcon + ' '
                + treeConfig.cssExpandedFolderIcon);
            node['is_leaf'] = false;
            var children = data.children;
            for (var i = 0; i < children.length; i++) {
              var child = children[i];
              var childNode = node.getTree().createNode('');
              node.add(childNode);
              createTreeFromData(childNode, child);
            }
          }
        };

        $scope.$watch(attrs.data, function(data) {
          tree.removeChildren(true);
          tree.setHtml('');
          if (data != '') {
            // TODO: angularjs watch may cause handler executed twice.
            // Should find a way to avoid create tree for same data.
            createTreeFromData(tree, data);
          }
        });
      }
    };
  };
});
