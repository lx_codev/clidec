goog.provide('clidec.ButtonMenuDrct');

goog.require('goog.ui.Menu');
goog.require('goog.ui.FlatMenuButtonRenderer');
goog.require('goog.ui.MenuButton');
goog.require('goog.ui.MenuItem');
goog.require('goog.ui.SubMenu');
goog.require('goog.ui.Separator');
goog.require('goog.ui.decorate');

goog.scope(function() {

  clidec.ButtonMenuDrct = function($parse) {
    return {
      restrict : 'AC',
      replace : true,
      link : function($scope, $element, attrs) {

        adaptClosureComponent = function(component) {
          var element = component.getElement();
          if (element.hasAttribute('ng-disabled')) {
            if (!component.setEnabled) { // component has setEnabled method
              console.log('there is not setEnabled method in component: '
                  + component);
            } else {
              $scope.$watch(element.getAttribute('ng-disabled'), function(
                  isDisabled) {
                if (goog.isDef(isDisabled)) {
                  component.setEnabled(!isDisabled);
                }
              });
            }
          }

          for (var i = 0; i < element.attributes.length; ++i) {
            var attr = element.attributes[i];
            if (attr.name.indexOf('ng-on-') == 0) {
              var eventName = attr.name.slice(6).toLowerCase();
              var handler = $parse(attr.value);
              goog.events.listen(component, eventName, function(e) {
                $scope.$apply(function() {
                  handler($scope, {
                    '$event' : e
                  });
                });
              });
              // todo(lx): do we need to consider unlisten the event when
              // componenta is destroyed.
            }
          }
        };

        adaptClosureComponentRecursively = function(component) {
          goog.bind(adaptClosureComponent, this)(component);
          component.forEachChild(adaptClosureComponentRecursively, this);
        };

        setGoogClass = function(element) {
          var newClassName = element.className.replace('ng-button-menu',
              'goog-flat-menu-button');
          goog.dom.setProperties(element, {
            'class' : newClassName
          });
        };

        setGoogClass($element[0]);
        var menuButton = goog.ui.decorate($element[0]);
        adaptClosureComponent(menuButton);
        var menu = menuButton.getMenu();
        goog.bind(adaptClosureComponentRecursively, $scope)(menu);
      }
    };
  };
});
