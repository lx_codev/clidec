goog.provide('clidec.FooterCtrl');

goog.require('goog.ui.Component');
goog.require('goog.Timer');

goog.scope(function() {

  clidec.FooterCtrl = function($scope) {
    $scope['footerCtrl'] = this;

    this.$scope_ = $scope;

    this.filePath = '';

    this.taskName = 'Task';

    this.progress = 0;

    this.max = 100;

    this.delta = 20;

    this.init();
  };

  var FooterCtrl = clidec.FooterCtrl;

  FooterCtrl.prototype.init = function() {
    this.$scope_.$on('EDITOR_FILE_PATH_UPDATE', goog.bind(
        this.onFilePathUpdate_, this));
    this.$scope_.$on('CONSOLE_LOG_OUTPUT', goog.bind(function(event, args) {
      this.progress = 0;
      this.max = 100;
    }, this));
    this.$scope_.$on('CONSOLE_LOG_UPDATE', goog.bind(function(event, args) {
      if (this.progress + this.delta >= this.max) {
        this.max += this.delta;
      }
      this.progress = ((this.progress + this.delta) * 100 / this.max) | 0;
      if (!this.$scope_.$$phase) {
        this.$scope_.$apply();
      }
    }, this));
  };

  FooterCtrl.prototype.onFilePathUpdate_ = function(event, args) {
    this.filePath = args.filePath;
    if (!this.$scope_.$$phase) {
      this.$scope_.$apply();
    }
  };
});
