goog.provide('clidec.EditorCtrl');

goog.require('clidec.common');

goog.scope(function() {

  clidec.EditorCtrl = function($scope, $rootScope, $http, WorkspaceService) {
    $scope['editorCtrl'] = this;

    this.$scope_ = $scope;

    this.$rootScope_ = $rootScope;

    this.$http_ = $http;

    this.$wsServ_ = WorkspaceService;

    this.code = 'demo code';

    this.tabBar = null;

    this.openedFiles = {};

    this.editors = null;

    this.currentFilePath = '';

    this.isRemoteLinting = false;

    this.init_();
  };

  var EditorCtrl = clidec.EditorCtrl;

  EditorCtrl.prototype.init_ = function() {
    this.$scope_.$on('CODE_LOADED', goog.bind(this.onCodeLoaded_, this));
    this.$scope_.$on('SAVE_FILE', goog.bind(this.save_, this));
    this.$scope_.$on('SHOW_DIFF', goog.bind(this.showDiff_, this));
  };

  EditorCtrl.prototype.refresh = function(response) {
  };

  EditorCtrl.prototype.javaValidator = function(text, updateLinting, options,
      cm) {
    if (this.isRemoteLinting) {
      return;
    }
    this.isRemoteLinting = true;
    var parseErrors = function(errors, output) {
      for (var i = 0; i < errors.length; i++) {
        var error = errors[i];
        if (error)
          output.push({
            message : error.msg,
            severity : 'error',
            from : CodeMirror.Pos(error.line - 1, error.start - 1),
            to : CodeMirror.Pos(error.line - 1, error.end)
          });
      }
    };
    var url = 'rest/fs/compile_file/' + options.filepath;
    this.$http_.get(url).success(goog.bind(function(response) {
      if (response) {
        var compileErrors = [];
        parseErrors(response.compileError, compileErrors);
        updateLinting(cm, compileErrors);
      }
      this.isRemoteLinting = false;
    }, this));
  };

  EditorCtrl.prototype.onCodeLoaded_ = function(event, args) {
    if (this.openedFiles.hasOwnProperty(args.filePath)) {
      this.tabBar.setSelectedTab(this.openedFiles[args.filePath]);
    } else {
      var index = args.filePath.lastIndexOf("/");
      var filename = args.filePath.substr(index + 1);
      var tab = this.tabBar.createTab();
      tab.setContent(filename);
      tab.setTooltip(args.filePath);
      this.openedFiles[args.filePath] = tab;
      this.addEditor(args.filePath, args.content);
      this.tabBar.addChildAt(tab, this.tabBar.getChildCount(), true);
      this.tabBar.setSelectedTab(tab);
    }
    this.currentFilePath = args.filePath;
    this.showEditor(this.currentFilePath);
  };

  EditorCtrl.prototype.switchDiff = function() {
    if (clidec.common.stringEndsWith(this.currentFilePath, "#head")) {
      this.showDiff();
    } else {

    }
  };

  EditorCtrl.prototype.showDiff_ = function() {
    var filePath = this.currentFilePath + "#head";
    if (this.openedFiles.hasOwnProperty(filePath)) {
      this.tabBar.setSelectedTab(this.openedFiles[filePath]);
    } else {
      var currentContent = this.code;
      var url = 'rest/gt/head/' + this.currentFilePath;
      this.$http_.get(url).success(goog.bind(function(response) {
        var index = filePath.lastIndexOf("/");
        var filename = filePath.substr(index + 1);
        var tab = this.tabBar.createTab();
        tab.setContent(filename);
        tab.setTooltip(filePath);
        this.openedFiles[filePath] = tab;
        this.addDiffViewer(filePath, currentContent, response);
        this.tabBar.addChildAt(tab, this.tabBar.getChildCount(), true);
        this.tabBar.setSelectedTab(tab);
      }, this));
    }
    this.editors.showEditor(filePath, false);
  };

  EditorCtrl.prototype.addDiffViewer = function(filePath, currentContent,
      originContent) {
    this.editors.addDiffViewer(filePath, {}, currentContent, originContent);
  };

  EditorCtrl.prototype.addEditor = function(filePath, content) {
    var config = {};
    if (clidec.common.getFileExtension(filePath) == 'java') {
      var validator = function(text, updateLinting, options, cm) {
        options.javaValidator(text, updateLinting, options, cm);
      };
      config['lint'] = {
        'getAnnotations' : validator,
        'async' : true,
        'javaValidator' : goog.bind(this.javaValidator, this),
        'filepath' : filePath
      };
    }
    var editor = this.editors.addEditor(filePath, config, content);
    editor.on("change", goog.bind(function(editor, change) {
      // TODO(lx): response on code change event.
      console.log(change);
    }, this));
    editor.on('cursorActivity', goog.bind(function(editor) {
      var cursor = editor.getDoc().getCursor();
      this.$rootScope_.$broadcast('EDITOR_POSITION_UPDATE', {
        line : cursor.line,
        ch : cursor.ch
      });
    }, this));
    this.editors.showEditor(filePath);
  };

  EditorCtrl.prototype.getLanguageMode = function(filePath) {
    var ext = clidec.common.getFileExtension(filePath);
    if (EditorCtrl.MODES.hasOwnProperty(ext)) {
      return EditorCtrl.MODES[ext];
    }
    return 'text';
  };

  EditorCtrl.prototype.hasLanguageLinter = function(filePath) {
    var ext = clidec.common.getFileExtension(filePath);
    return EditorCtrl.LINTERS.indexOf(ext) != -1;
  };

  EditorCtrl.prototype.onSelectTab = function(tab) {
    if (tab) {
      if (this.currentFilePath && this.editors
          && this.currentFilePath == this.editors.current
          && this.code != this.editors.getContent(this.currentFilePath)) {
        this.save_();
      }
      this.currentFilePath = tab.getTooltip();
      this.showEditor(this.currentFilePath);
      this.$rootScope_.$broadcast('EDITOR_FILE_PATH_UPDATE', {
        filePath : this.currentFilePath
      });
    }
  };

  EditorCtrl.prototype.onCloseTab = function(tab) {
    if (tab) {
      if (this.currentFilePath && this.editors
          && this.currentFilePath == this.editors.current
          && this.code != this.editors.getContent(this.currentFilePath)) {
        this.save_();
      }
      delete this.openedFiles[tab.getTooltip()];
      this.editors.removeEditor(tab.getTooltip());
      this.currentFilePath = '';
      for ( var path in this.openedFiles) {
        if (this.openedFiles.hasOwnProperty(path)) {
          this.currentFilePath = path;
          this.editors.showEditor(this.currentFilePath);
          break;
        }
      }
    }
  };

  EditorCtrl.prototype.showEditor = function(filePath) {
    this.code = this.editors.getContent(filePath);
    this.editors.showEditor(filePath, false);
  };

  EditorCtrl.prototype.save_ = function() {
    console.log('saving...');
    this.code = this.editors.getContent(this.currentFilePath);
    var url = 'rest/fs/write_file/' + this.currentFilePath;
    this.$http_.post(url, this.code).success(goog.bind(this.refresh, this));
  };
});