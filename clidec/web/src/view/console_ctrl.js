goog.provide('clidec.ConsoleCtrl');

goog.scope(function() {

  clidec.ConsoleCtrl = function($scope, $http) {
    $scope['consoleCtrl'] = this;

    this.$scope_ = $scope;

    this.$http_ = $http;

    this.output = 'test output';

    this.init();
  };

  var ConsoleCtrl = clidec.ConsoleCtrl;

  ConsoleCtrl.prototype.init = function() {
    this.$scope_.$on('CONSOLE_LOG_OUTPUT', goog.bind(function(event, args) {
      this.refresh_(args.log);
    }, this));
    this.$scope_.$on('CONSOLE_LOG_UPDATE', goog.bind(function(event, args) {
      this.refresh_(args.log);
    }, this));
  };

  ConsoleCtrl.prototype.refresh_ = function(log) {
    this.output = log;
  };
});
