goog.provide('clidec.MavenCtrl');

goog.require('clidec.common');
goog.require('clidec.http');

goog.scope(function() {

  clidec.MavenCtrl = function($scope, $rootScope, $q, $http, ProjectService) {
    $scope['mavenCtrl'] = this;

    this.$scope_ = $scope;

    this.$rootScope_ = $rootScope;

    this.$q_ = $q;

    this.$http_ = $http;

    this.$pjServ_ = ProjectService;

    this.currentWorkspace = '';

    this.last = {
      goals : '',
      project : '',
      parameters : ''
    };

    this.next = {
      goals : '',
      project : '',
      parameters : ''
    };

    this.projects = '';

    this.init();
  };

  var MavenCtrl = clidec.MavenCtrl;

  MavenCtrl.prototype.init = function() {
    this.$scope_.$on('PROJECT_UPDATE', goog.bind(function(event, args) {
      this.update_(args);
    }, this));
  };

  MavenCtrl.prototype.update_ = function(args) {
    if (this.currentWorkspace != args.workspace) {
      this.currentWorkspace = args.workspace;
      this.last = {
        goals : '',
        project : '',
        parameters : ''
      };
    }
    this.projects = args.projects;
  };

  MavenCtrl.prototype.requestExecution = function(goal, parameters) {
    this.next = {
      goals : goal || '',
      project : this.$pjServ_.getCurrentProject(),
      parameters : parameters || ''
    };
    this.$scope_.request_goal_execution_dialog.open();
  };

  MavenCtrl.prototype.execute = function() {
    this.executeGoalStream_(this.next);
  };

  MavenCtrl.prototype.reexecute = function() {
    this.executeGoalStream_(this.last);
  };

  MavenCtrl.prototype.executeGoal_ = function(target) {
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : ''
    });
    var goals = target.goals.replace(/ /g, '+').replace(/:/g, '%3A');
    var url = 'rest/mv/' + goals + '/' + this.currentWorkspace + '/'
        + target.project;
    var parameters = clidec.common.parseParameters(target.parameters);
    this.$http_.post(url, parameters).success(goog.bind(function(response) {
      this.last = {
        goals : target.goals,
        project : target.project,
        parameters : target.parameters
      };
      this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
        log : response
      });
    }, this));
  };

  MavenCtrl.prototype.executeGoalStream_ = function(target) {
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : ''
    });
    var goals = target.goals.replace(/ /g, '+').replace(/:/g, '%3A');
    var url = 'rest/mv/' + goals + '/' + this.currentWorkspace + '/'
        + target.project;
    var parameters = clidec.common.parseParameters(target.parameters);
    clidec.http.stream(this.$q_, url, parameters).then(
        goog.bind(function(response) {
          this.last = {
            goals : target.goals,
            project : target.project,
            parameters : target.parameters
          };
          this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
            log : response
          });
        }, this), // onSuccess
        angular.noop, // onError
        goog.bind(function(response) { // onProgress
          if (response) {
            this.$rootScope_.$broadcast('CONSOLE_LOG_UPDATE', {
              log : response
            });
            if (!this.$scope_.$$phase) {
              this.$scope_.$apply();
            }
          }
        }, this));
  };

  MavenCtrl.prototype.cancel = function() {
  };

});
