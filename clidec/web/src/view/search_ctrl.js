goog.provide('clidec.SearchCtrl');

goog.require('clidec.common');
goog.require('clidec.http');

goog.scope(function() {

  clidec.SearchCtrl = function($scope, $rootScope, $q, $http, ProjectService) {
    $scope['searchCtrl'] = this;

    this.$scope_ = $scope;

    this.$rootScope_ = $rootScope;

    this.$q_ = $q;

    this.$http_ = $http;

    this.$pjServ_ = ProjectService;

    this.currentWorkspace = '';

    this.query = '';

    this.init();
  };

  var SearchCtrl = clidec.SearchCtrl;

  SearchCtrl.prototype.init = function() {
    this.$scope_.$on('PROJECT_UPDATE', goog.bind(function(event, args) {
      this.update_(args);
    }, this));
  };

  SearchCtrl.prototype.update_ = function(args) {
    if (this.currentWorkspace != args.workspace) {
      this.currentWorkspace = args.workspace;
      this.query = '';
    }
    this.projects = args.projects;
  };

  SearchCtrl.prototype.search = function(target) {
    this.searchByGrok(target);
  };

  SearchCtrl.prototype.searchByGrep = function(target) {
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : ''
    });
    var escapedQuery = this.query.replace(/ /g, '+').replace(/:/g, '%3A');
    var url = 'rest/sc/grep/' + this.$pjServ_.getSelectedPath();
    this.$http_.post(url, escapedQuery).success(goog.bind(function(response) {
      this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
        log : response
      });
    }, this));
  };

  SearchCtrl.prototype.searchByGrok = function(target) {
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : ''
    });
    var escapedQuery = this.query.replace(/ /g, '+').replace(/:/g, '%3A');
    var url = 'http://localhost:18080/source/json?freetext=' + escapedQuery;
    this.$http_.get(url).success(goog.bind(this.displayGrokResult_, this));
  };

  SearchCtrl.prototype.searchCorsByGrok = function(target) {
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : ''
    });
    var escapedQuery = this.query.replace(/ /g, '+').replace(/:/g, '%3A');
    var url = 'http://localhost:18080/source/json?freetext=' + escapedQuery;
    this.$http_.get(url).success(goog.bind(this.displayGrokResult_, this));
    clidec.http.cors(this.$q_, url, 'get', {}).success(
        goog.bind(this.displayGrokResult_, this));
  };

  SearchCtrl.prototype.displayGrokResult_ = function(response) {
    var results = '';
    goog.array.forEach(response.results, function(result) {
      results += result.path.trim() + ' [line:' + result.lineno + ']:\n';
      var line = atob(result.line).replace(/(?:\r\n|\r|\n)/g, ' ');
      results += line + '\n';
    });
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : results
    });
  };

  SearchCtrl.prototype.cancel = function() {
  };

});
