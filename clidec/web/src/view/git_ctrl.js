goog.provide('clidec.GitCtrl');

goog.require('clidec.common');
goog.require('clidec.http');

goog.scope(function() {

  clidec.GitCtrl = function($scope, $rootScope, $q, $http, ProjectService) {
    $scope['gitCtrl'] = this;

    this.$scope_ = $scope;

    this.$rootScope_ = $rootScope;

    this.$q_ = $q;

    this.$http_ = $http;

    this.$pjServ_ = ProjectService;

    this.currentWorkspace = '';

    this.repositories = '';

    this.last = {
      repository : '',
      command : '',
      parameters : []
    };

    this.next = {
      repository : '',
      command : '',
      parameters : []
    };

    this.newParamKey = '';

    this.newParamValue = '';

    this.init();
  };

  var GitCtrl = clidec.GitCtrl;

  GitCtrl.prototype.Commands = {
    'status' : {},
    'log' : {},
    'add' : {
      parameters : [ {
        key : 'default',
        value : '',
        required : true
      } ]
    },
    'commit' : {
      parameters : [ {
        key : 'default',
        value : '',
        required : true
      } ]
    },
    'clone' : {
      noRepositories : true,
      parameters : [ {
        key : 'url',
        value : '',
        required : true
      }, {
        key : 'remote name',
        value : 'origin',
        required : true
      }, {
        key : 'username',
        value : ''
      }, {
        key : 'password',
        value : '',
        type : 'password'
      }, {
        key : 'path',
        value : ''
      } ]
    },
    'remote add' : {
      parameters : [ {
        key : 'remote name',
        value : 'origin',
        required : true
      }, {
        key : 'url',
        value : '',
        required : true
      }, {
        key : 'username',
        value : ''
      }, {
        key : 'password',
        value : '',
        type : 'password'
      } ]
    },
    'remote remove' : {
      parameters : [ {
        key : 'remote name',
        value : 'origin'
      } ]
    },
    'push' : {
      parameters : [ {
        key : 'remote name',
        value : 'origin',
        required : true
      }, {
        key : 'brach name',
        value : 'master',
        required : true
      } ]
    },
    'pull' : {
      parameters : [ {
        key : 'remote name',
        value : 'origin',
        required : true
      }, {
        key : 'brach name',
        value : 'master',
        required : true
      } ]
    }
  };

  GitCtrl.prototype.init = function() {
    this.$scope_.$on('PROJECT_UPDATE', goog.bind(function(event, args) {
      if (this.currentWorkspace != args.workspace) {
        this.currentWorkspace = args.workspace;
        this.last = {
          repository : '',
          command : '',
          parameters : []
        };
      }
      this.refresh_();
    }, this));
  };

  GitCtrl.prototype.refresh_ = function() {
    if (this.currentWorkspace != '') {
      this.$http_.get('rest/gt/ls/' + this.currentWorkspace).success(
          goog.bind(function(response) {
            this.repositories = response;
          }, this));
    } else {
      this.repositories = [];
    }
  };

  GitCtrl.prototype.requestInit = function() {
    this.selectedPath = this.$pjServ_.getSelectedPath();
    this.$scope_.request_init_dialog.open();
  };

  GitCtrl.prototype.initRepository = function() {
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : ''
    });
    var url = 'rest/gt/init/' + this.selectedPath;
    this.$http_.post(url, {}).success(goog.bind(function(response) {
      this.$rootScope_.$broadcast('WORKSPACE_UPDATE', {
        current : this.currentWorkspace
      });
      this.refresh_();
      this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
        log : response
      });
    }, this));
  };

  GitCtrl.prototype.requestExecution = function(command) {
    this.next = {
      repository : this.last.repository,
      command : command ? command : this.last.command,
      parameters : command ? this.Commands[command].parameters : {}
    };
    this.$scope_.request_execution_dialog.open();
  };

  GitCtrl.prototype.execute = function() {
    this.executeCommandStream_(this.next);
  };

  GitCtrl.prototype.reexecute = function() {
    this.executeCommandStream_(this.last);
  };

  GitCtrl.prototype.executeCommand_ = function(target) {
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : ''
    });
    if (!this.Commands.hasOwnProperty(target.command)) {
      this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
        log : 'invalid command: ' + target.command
      });
      return;
    }
    var jsonParameters = this.makeJsonParameters(target.parameters);
    var url = 'rest/gt/' + target.command.replace(/ /g, '_') + '/'
        + this.currentWorkspace + '/' + target.repository;
    this.$http_.post(url, jsonParameters).success(goog.bind(function(response) {
      this.last = {
        repository : target.repository,
        command : target.command,
        parameters : target.parameters
      };
      // TODO(lx): check whether need to update workspace.
      this.$rootScope_.$broadcast('WORKSPACE_UPDATE', {
        current : this.currentWorkspace
      });
      this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
        log : response
      });
    }, this));
  };

  GitCtrl.prototype.executeCommandStream_ = function(target) {
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : ''
    });
    if (!this.Commands.hasOwnProperty(target.command)) {
      this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
        log : 'invalid command: ' + target.command
      });
      return;
    }
    var jsonParameters = this.makeJsonParameters(target.parameters);
    var url = 'rest/gt/' + target.command.replace(/ /g, '_') + '/'
        + this.currentWorkspace + '/' + target.repository;
    clidec.http.stream(this.$q_, url, jsonParameters).then(
        goog.bind(function(response) {
          this.last = {
            repository : target.repository,
            command : target.command,
            parameters : target.parameters
          };
          // TODO(lx): check whether need to update workspace.
          this.$rootScope_.$broadcast('WORKSPACE_UPDATE', {
            current : this.currentWorkspace
          });
          this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
            log : response
          });
          if (!this.$scope_.$$phase) {
            this.$scope_.$apply();
          }
        }, this), // onSuccess
        angular.noop, // onError
        goog.bind(function(response) { // onProgress
          if (response) {
            this.$rootScope_.$broadcast('CONSOLE_LOG_UPDATE', {
              log : response
            });
            if (!this.$scope_.$$phase) {
              this.$scope_.$apply();
            }
          }
        }, this));
  };

  GitCtrl.prototype.makeJsonParameters = function(parameters) {
    var jsonParameters = {};
    for (var i = 0; i < (parameters ? parameters.length : 0); ++i) {
      jsonParameters[parameters[i].key] = parameters[i].value;
    }
    return jsonParameters;
  };

  GitCtrl.prototype.addParameter = function() {
    this.next.parameters.push({
      key : this.newParamKey,
      value : this.newParamValue
    });
  };

  GitCtrl.prototype.removeParameter = function(index) {
    this.next.parameters.splice(index, 1);
  };

  GitCtrl.prototype.cancel = function() {
  };

});
