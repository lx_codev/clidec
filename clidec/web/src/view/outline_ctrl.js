goog.provide('clidec.OutlineCtrl');

goog.require('clidec.common');

goog.scope(function() {

  clidec.OutlineCtrl = function($scope, $rootScope, $http, ProjectService) {
    $scope['outlineCtrl'] = this;

    this.$scope_ = $scope;

    this.$rootScope_ = $rootScope;

    this.$http_ = $http;

    this.$pjServ_ = ProjectService;

    this.currentWorkspace = '';

    this.currentFilePath = '';

    this.ast = [];

    this.astTree = null;

    this.seleted = {};

    this.init_();
  };

  var OutlineCtrl = clidec.OutlineCtrl;

  OutlineCtrl.prototype.init_ = function() {
    this.$scope_.$on('CODE_LOADED', goog.bind(this.onCodeLoaded_, this));
  };

  OutlineCtrl.prototype.onCodeLoaded_ = function(event, args) {
    if (clidec.common.getFileExtension(args.filePath) == "java") {
      var url = 'rest/js/parse/' + args.filePath;
      this.$http_.get(url).success(goog.bind(function(response) {
        this.ast = response;
        if (this.astTree) {
          this.astTree.expand();
        }
      }, this));
    } else {
      this.ast = [];
    }
  };

  OutlineCtrl.prototype.onSelected = function($selected) {
    this.seleted = $selected;
    console.log($selected.path);
  };
});
