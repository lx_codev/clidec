goog.provide('clidec.PositionCtrl');

goog.scope(function() {

  clidec.PositionCtrl = function($scope) {
    $scope['positionCtrl'] = this;

    this.$scope_ = $scope;

    this.line = 0;

    this.ch = 0;

    this.init();
  };

  var PositionCtrl = clidec.PositionCtrl;

  PositionCtrl.prototype.init = function() {
    this.$scope_.$on('EDITOR_POSITION_UPDATE', goog.bind(function(event, args) {
      this.refresh_(args.line, args.ch);
      if (!this.$scope_.$$phase) {
        this.$scope_.$apply();
      }
    }, this));
  };

  PositionCtrl.prototype.refresh_ = function(line, ch) {
    this.line = line;
    this.ch = ch;
  };
});
