goog.provide('clidec.FileTreeCtrl');

goog.scope(function() {

  clidec.FileTreeCtrl = function($scope, $rootScope, $http, ProjectService) {
    $scope['fileTreeCtrl'] = this;

    this.$scope_ = $scope;

    this.$rootScope_ = $rootScope;

    this.$http_ = $http;

    this.$pjServ_ = ProjectService;

    this.currentWorkspace = '';

    this.files = [];

    this.fileTree = null;

    this.seletedNode = {};

    this.selectedPath = '';

    this.inputName = '';

    this.init_();
  };

  var FileTreeCtrl = clidec.FileTreeCtrl;

  FileTreeCtrl.prototype.init_ = function() {
    this.$scope_.$on('PROJECT_UPDATE', goog.bind(function(event, args) {
      this.currentWorkspace = args.workspace;
      this.update_(args.files);
    }, this));
    this.currentWorkspace = this.$pjServ_.getCurrentWorkspace();
    this.update_(this.$pjServ_.getAllFiles());
  };

  FileTreeCtrl.prototype.update_ = function(response) {
    this.files = response;
    if (this.fileTree) {
      this.fileTree.expand();
    }
  };

  FileTreeCtrl.prototype.addFile = function($selected) {
    this.$scope_.new_file_name_dialog.open();
  };

  FileTreeCtrl.prototype.removeFile = function($selected) {
    var url = 'rest/fs/remove_file/' + this.selectedPath;
    this.$http_.get(url).success(goog.bind(this.update_, this));
  };

  FileTreeCtrl.prototype.addFolder = function($selected) {
    this.$scope_.new_folder_name_dialog.open();
  };

  FileTreeCtrl.prototype.removeFolder = function($selected) {
    if (this.selectedPath != this.currentWorkspace) {
      var url = 'rest/fs/remove_folder/' + this.selectedPath;
      this.$http_.get(url).success(goog.bind(function(response) {
        var parentNode = this.seleted.parent_;
        var isProject = parentNode && (parentNode == this.fileTree);
        this.update_(response);
        if (isProject) {
          this.$rootScope_.$broadcast('WORKSPACE_UPDATE', {
            current : this.currentWorkspace
          });
        }
      }, this));
    }
  };

  FileTreeCtrl.prototype.newFile = function() {
    var url = 'rest/fs/add_file/' + this.selectedPath + "/" + this.inputName;
    this.$http_.get(url).success(goog.bind(this.update_, this));
    this.inputName = '';
  };

  FileTreeCtrl.prototype.newFolder = function() {
    var url = 'rest/fs/add_folder/' + this.selectedPath + "/" + this.inputName;
    this.$http_.get(url).success(goog.bind(function(response) {
      this.update_(response);
      if (this.selectedPath === this.currentWorkspace) {
        this.$rootScope_.$broadcast('WORKSPACE_UPDATE', {
          current : this.currentWorkspace
        });
      }
    }, this));
    this.inputName = '';
  };

  FileTreeCtrl.prototype.cancel = function() {
  };

  FileTreeCtrl.prototype.onSelected = function($selected) {
    this.seleted = $selected;
    this.selectedPath = $selected.path;
    this.$pjServ_.setSelectedPath(this.selectedPath);
  };

  FileTreeCtrl.prototype.loadFile = function($selected) {
    if ($selected.is_leaf) {
      var url = 'rest/fs/read_file/' + $selected.path;
      this.$http_.get(url).success(
          goog.bind(this.openedFile_, this, $selected.path));
    }
  };

  FileTreeCtrl.prototype.openedFile_ = function(path, response) {
    this.$rootScope_.$broadcast('CODE_LOADED', {
      filePath : path,
      content : response,
    });
  };
});
