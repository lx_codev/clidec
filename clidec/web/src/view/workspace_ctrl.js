goog.provide('clidec.WorkspaceCtrl');

goog.scope(function() {

  clidec.WorkspaceCtrl = function($scope, $http, WorkspaceService) {
    $scope['workspaceCtrl'] = this;

    this.$scope_ = $scope;

    this.$http_ = $http;

    this.$wsServ_ = WorkspaceService;

    this.workspaces = [];

    this.currentWorkspace = '';

    this.targetWorkspace = '';

    this.newWorkspace = '';

    this.init();
  };

  var WorkspaceCtrl = clidec.WorkspaceCtrl;

  WorkspaceCtrl.prototype.init = function() {
    this.$scope_.$on('WORKSPACE_UPDATE', goog.bind(this.refresh_, this));
    this.workspaces = this.$wsServ_.getAllWorkspaces();
  };

  WorkspaceCtrl.prototype.refresh_ = function(event, args) {
    this.workspaces = this.$wsServ_.getAllWorkspaces();
    this.currentWorkspace = args.current;
  };

  WorkspaceCtrl.prototype.requestGo = function() {
    this.$scope_.select_workspace_dialog.open();
  };

  WorkspaceCtrl.prototype.requestAdd = function() {
    this.$scope_.new_workspace_dialog.open();
  };

  WorkspaceCtrl.prototype.requestRemove = function() {
    this.$scope_.remove_workspace_dialog.open();
  };

  WorkspaceCtrl.prototype.go = function() {
    this.$wsServ_.goToWorkspace(this.targetWorkspace);
  };

  WorkspaceCtrl.prototype.add = function() {
    var index = this.workspaces.indexOf(this.newWorkspace);
    if (index < 0) {
      this.$wsServ_.addWorkspace(this.newWorkspace);
      this.newWorkspace = '';
    } else {
      alert("Workspace '" + this.newWorkspace + "' existing!");
    }
  };

  WorkspaceCtrl.prototype.cancel = function() {
  };

  WorkspaceCtrl.prototype.remove = function() {
    if (this.currentWorkspace) {
      this.$wsServ_.removeWorkspace(this.currentWorkspace);
    }
  };
});
