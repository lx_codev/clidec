goog.provide('clidec.TomcatCtrl');

goog.scope(function() {

  clidec.TomcatCtrl = function($scope, $rootScope, $http, ProjectService) {
    $scope['tomcatCtrl'] = this;

    this.$scope_ = $scope;

    this.$rootScope_ = $rootScope;

    this.$http_ = $http;

    this.$pjServ_ = ProjectService;

    this.currentWorkspace = '';

    this.last = {
      op : '',
      path : '',
      parameters : ''
    };

    this.next = {
      op : '',
      path : '',
      parameters : ''
    };

    this.projects = '';

    this.init();
  };

  var TomcatCtrl = clidec.TomcatCtrl;

  TomcatCtrl.prototype.init = function() {
    this.$scope_.$on('PROJECT_UPDATE', goog.bind(function(event, args) {
      this.update_(args);
    }, this));
  };

  TomcatCtrl.prototype.update_ = function(args) {
    if (this.currentWorkspace != args.workspace) {
      this.currentWorkspace = args.workspace;
      this.last = {
        op : '',
        path : '',
        parameters : ''
      };
    }
    this.projects = args.projects;
  };

  TomcatCtrl.prototype.requestRunProject = function() {
    this.next = {
      op : 'run',
      path : this.$pjServ_.getCurrentProject(),
      parameters : ''
    };
    this.$scope_.request_run_dialog.open();
  };

  TomcatCtrl.prototype.requestDeploy = function() {
    this.next = {
      op : 'deploy',
      path : this.$pjServ_.getSelectedWorkspacePath(),
      parameters : ''
    };
    this.$scope_.request_deploy_dialog.open();
  };

  TomcatCtrl.prototype.start = function() {
    this.next = {
      op : 'start',
      path : '',
      parameters : ''
    };
    this.execute_(this.next);
  };

  TomcatCtrl.prototype.deploy = function() {
    if (clidec.common.getFileExtension(this.next.path) != 'war') {
      this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
        log : 'Can only deploy war file! Invalid path: ' + this.next.path
      });
    } else {
      this.execute_(this.next);
    }
  };

  TomcatCtrl.prototype.stop = function() {
    this.next = {
      op : 'stop',
      path : '',
      parameters : ''
    };
    this.execute_(this.next);
  };

  TomcatCtrl.prototype.execute = function() {
    this.execute_(this.next);
  };

  TomcatCtrl.prototype.reexecute = function() {
    this.execute_(this.last);
  };

  TomcatCtrl.prototype.execute_ = function(target) {
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : ''
    });
    var url = 'rest/tc/' + target.op + '/' + this.currentWorkspace + '/'
        + target.path;
    var parameters = clidec.common.parseParameters(target.parameters);
    this.$http_.post(url, parameters).success(goog.bind(function(response) {
      this.last = {
        op : target.op,
        path : target.path,
        parameters : target.parameters
      };
      this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
        log : response
      });
    }, this));
  };

  TomcatCtrl.prototype.cancel = function() {
  };

});
