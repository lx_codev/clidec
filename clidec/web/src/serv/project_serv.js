goog.provide('clidec.ProjectServ');

goog.scope(function() {

  clidec.ProjectServ = function($http, $rootScope, WorkspaceService) {
    this.$http_ = $http;

    this.$rootScope_ = $rootScope;

    this.$wsServ_ = WorkspaceService;

    this.currentWorkspace = '';

    this.files = [];

    this.projects = [];

    this.currentProject = '';

    this.selectedProjectPath = '';

    this.init_();
  };

  var ProjectServ = clidec.ProjectServ;

  ProjectServ.prototype.init_ = function() {
    this.refresh_(this.$wsServ_.getCurrentWorkspace());
    this.$rootScope_.$on('WORKSPACE_UPDATE', goog.bind(function(event, args) {
      if (this.currentWorkspace != args.current) {
        this.refresh_(args.current);
      }
    }, this));
  };

  ProjectServ.prototype.refresh_ = function(current) {
    this.currentWorkspace = current;
    if (this.currentWorkspace != '') {
      var url = 'rest/fs/' + this.currentWorkspace;
      this.$http_.get(url).success(goog.bind(this.update_, this));
    } else {
      this.update_([]);
    }
  };

  ProjectServ.prototype.update_ = function(response) {
    this.files = response;
    this.projects = [];
    if (this.files.children) {
      var children = this.files.children;
      for (var i = 0; i < children.length; ++i) {
        if (children[i].children) {
          this.projects.push(children[i].id);
        }
      }
    }
    this.$rootScope_.$broadcast('PROJECT_UPDATE', {
      workspace : this.currentWorkspace,
      files : this.files,
      projects : this.projects,
      current : this.currentProject,
    });
  };

  ProjectServ.prototype.getAllFiles = function() {
    return this.files;
  };

  ProjectServ.prototype.getAllProjects = function() {
    return this.projects;
  };

  ProjectServ.prototype.getCurrentWorkspace = function() {
    return this.currentWorkspace;
  };

  ProjectServ.prototype.setSelectedPath = function(path) {
    if (path) {
      var projectPath = path.substring(this.currentWorkspace.length + 1);
      this.currentProject = projectPath ? projectPath.split("/")[0] : '';
      this.selectedProjectPath = projectPath
          .substring(this.currentProject.length + 1)
          || '';
    }
  };

  ProjectServ.prototype.getSelectedPath = function() {
    var path = this.currentWorkspace;
    if (this.currentProject) {
      path += '/' + this.currentProject;
      path += this.selectedProjectPath ? '/' + this.selectedProjectPath : '';
    }
    return path;
  };

  ProjectServ.prototype.getSelectedWorkspacePath = function() {
    var path = this.currentProject;
    if (this.currentProject) {
      path += this.selectedProjectPath ? '/' + this.selectedProjectPath : '';
    }
    return path;
  };

  ProjectServ.prototype.getSelectedProjectPath = function() {
    return this.selectedProjectPath;
  };

  ProjectServ.prototype.getCurrentProject = function() {
    return this.currentProject;
  };

  ProjectServ.prototype.addProject = function(project) {
    var url = 'rest/fs/add_folder/' + this.currentWorkspace + "/" + project;
    this.$http_.get(url).success(goog.bind(this.update_, this));
  };

  ProjectServ.prototype.removeProject = function(project) {
    var url = 'rest/fs/remove_folder/' + this.currentWorkspace + "/" + project;
    this.$http_.get(url).success(goog.bind(this.update_, this));
  };
});
