goog.provide('clidec.WorkspaceServ');

goog.require('clidec.common');

goog.scope(function() {

  clidec.WorkspaceServ = function($http, $rootScope) {
    this.$http_ = $http;

    this.$rootScope_ = $rootScope;

    this.workspaces = [];

    this.currentWorkspace = '';
  };

  var WorkspaceServ = clidec.WorkspaceServ;

  WorkspaceServ.prototype.getAllWorkspaces = function() {
    this.$http_.get('rest/ws').success(goog.bind(function(response) {
      if (clidec.common.array1Equals(this.workspaces, response)) {
        return;
      }
      this.workspaces = response;
      if (this.workspaces.length > 0) {
        var index = this.workspaces.indexOf(this.currentWorkspace);
        if (index < 0) {
          this.currentWorkspace = this.workspaces[0];
        }
      } else {
        this.currentWorkspace = '';
      }
      this.update_();
    }, this));
    return this.workspaces;
  };

  WorkspaceServ.prototype.update_ = function() {
    this.$rootScope_.$broadcast('WORKSPACE_UPDATE', {
      current : this.currentWorkspace,
    });
    this.$rootScope_.$broadcast('CONSOLE_LOG_OUTPUT', {
      log : 'Switched to workspace ' + this.currentWorkspace + '.'
    });
  };

  WorkspaceServ.prototype.getCurrentWorkspace = function() {
    return this.currentWorkspace;
  };

  WorkspaceServ.prototype.goToWorkspace = function(workspace) {
    this.$http_.post('rest/ws/get/' + workspace, {}).success(
        goog.bind(function(response) {
          this.currentWorkspace = response.name;
          this.update_();
        }, this));
  };

  WorkspaceServ.prototype.addWorkspace = function(workspace) {
    this.$http_.post('rest/ws/add/' + workspace, {}).success(
        goog.bind(function(response) {
          this.currentWorkspace = response.name;
          var index = this.workspaces.indexOf(this.currentWorkspace);
          if (index < 0) {
            this.workspaces.push(this.currentWorkspace);
          }
          this.update_();
        }, this));
  };

  WorkspaceServ.prototype.removeWorkspace = function(workspace) {
    this.$http_.post('rest/ws/remove/' + workspace, {}).success(
        goog.bind(function(response) {
          var workspace = response.name;
          var index = this.workspaces.indexOf(workspace);
          if (index >= 0) {
            this.workspaces.splice(index, 1);
            if (this.currentWorkspace == workspace) {
              this.currentWorkspace = '';
            }
            this.update_();
          }
        }, this));
  };
});
