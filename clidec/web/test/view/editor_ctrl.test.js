goog.require("clidec.EditorCtrl");

describe('clidec.EditorCtrl', function() {

  beforeEach(module('Clidec'));

  var $scope, createController;

  beforeEach(inject(function($rootScope) {
    $scope = $rootScope.$new();
  }));

  beforeEach(inject(function($controller) {
    createController = function() {
      return $controller('EditorCtrl', {
        '$scope' : $scope
      });
    };
  }));

  it('should set editor name', function() {
    var ctrl = createController();
    expect(ctrl.editorName).toBe("Clidec editor");
  });
});