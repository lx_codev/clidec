package clidec;

import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;

import clidec.model.CodeBase;
import clidec.model.GitBase;
import clidec.model.GitRepository;
import clidec.model.StandaloneTomcatServer;
import clidec.model.Workspaces;
import clidec.rest.FileService;
import clidec.rest.GitService;
import clidec.rest.JavaService;
import clidec.rest.MavenService;
import clidec.rest.SearchService;
import clidec.rest.TomcatService;
import clidec.rest.UserService;
import clidec.rest.WorkspacesService;
import clidec.util.JavaUtil;
import clidec.util.MavenUtil;
import clidec.util.ShellUtil;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class ClidecGuiceServletConfig extends GuiceServletContextListener {

  @Override
  protected Injector getInjector() {
    return Guice.createInjector(new JerseyServletModule() {

      @Override
      protected void configureServlets() {
        // bind the REST resources
        bind(CodeBase.class).asEagerSingleton();
        bind(EnvironmentConfig.class).asEagerSingleton();
        bind(GitBase.class).asEagerSingleton();
        bind(StandaloneTomcatServer.class).asEagerSingleton();
        bind(Workspaces.class).asEagerSingleton();

        bind(FileService.class);
        bind(GitService.class);
        bind(JavaService.class);
        bind(MavenService.class);
        bind(SearchService.class);
        bind(TomcatService.class);
        bind(WorkspacesService.class);
        bind(UserService.class);

        // bind jackson converters for JAXB/JSON serialization
        bind(MessageBodyReader.class).to(JacksonJsonProvider.class);
        bind(MessageBodyWriter.class).to(JacksonJsonProvider.class);

        requestStaticInjection(CodeBase.class);
        requestStaticInjection(FileService.class);
        requestStaticInjection(GitBase.class);
        requestStaticInjection(GitRepository.class);
        requestStaticInjection(GitService.class);
        requestStaticInjection(JavaService.class);
        requestStaticInjection(JavaUtil.class);
        requestStaticInjection(MavenService.class);
        requestStaticInjection(MavenUtil.class);
        requestStaticInjection(ShellUtil.class);
        requestStaticInjection(StandaloneTomcatServer.class);
        requestStaticInjection(SearchService.class);
        requestStaticInjection(TomcatService.class);
        requestStaticInjection(Workspaces.class);
        requestStaticInjection(WorkspacesService.class);

        serve("/rest/*").with(GuiceContainer.class);
      }
    });
  }
}