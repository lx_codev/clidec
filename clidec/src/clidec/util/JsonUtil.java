package clidec.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import com.google.inject.Inject;

public class JsonUtil {

  @Inject
  private static Logger logger;

  public static Map<String, String> parseJsonParameters(
      Map<String, String> jsonParameters) {
    Map<String, String> parameters = new HashMap<String, String>();
    @SuppressWarnings("unchecked")
    Iterator<String> keysItr = jsonParameters.keySet().iterator();
    while (keysItr.hasNext()) {
      String key = keysItr.next();
      parameters.put(key, jsonParameters.get(key).toString());
    }
    return parameters;
  }
}
