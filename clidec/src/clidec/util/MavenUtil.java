package clidec.util;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationOutputHandler;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;

import clidec.EnvironmentConfig;

public class MavenUtil {

  @Inject
  private static Logger logger;

  @Inject
  private static EnvironmentConfig config;

  public static class StreamingInvocationOutputHandler implements
      InvocationOutputHandler {
    Writer writer;
    StringBuilder builder = new StringBuilder();

    public StreamingInvocationOutputHandler(OutputStream stream) {
      writer = new OutputStreamWriter(stream);
    }

    @Override
    public void consumeLine(String line) {
      try {
        writer.write(line + "\n");
        writer.flush();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public static class StringBufferInvocationOutputHandler implements
      InvocationOutputHandler {
    StringBuilder builder = new StringBuilder();

    public void consumeLine(String line) {
      builder.append(line + "\n");
    }

    public void clear() {
      builder = new StringBuilder();
    }

    public String toString() {
      return builder.toString();
    }
  }

  private static Invoker getInvoker() {
    Invoker invoker = new DefaultInvoker();
    invoker.setMavenHome(new File(config.getMavenHome()));
    invoker.setLocalRepositoryDirectory(new File(config.getMavenRepository()));
    return invoker;
  }

  public static boolean generateArcheType() {
    InvocationRequest request = new DefaultInvocationRequest();
    request.setGoals(Collections.singletonList("archetype:generate"));
    request.setInteractive(false);
    Properties properties = new Properties();
    properties.setProperty("groupId", "com.maven");
    properties.setProperty("artifactId", "test");
    properties.setProperty("archetypeVersion", "1.0-alpha-4");
    properties.setProperty("archetypeGroupId", "org.apache.maven.archetypes");
    properties
        .setProperty("archetypeArtifactId", "maven-archetype-j2ee-simple");
    properties.setProperty("archetypeCatalog",
        "http://repo1.maven.org/maven2/archetype-catalog.xml");
    request.setProperties(properties);

    try {
      InvocationResult result = getInvoker().execute(request);
      if (result.getExitCode() != 0) {
        if (result.getExecutionException() != null) {
          logger.warning("Failed to execute goal: "
              + result.getExecutionException());
        } else {
          logger.warning("Failed to execute goal with exit code: "
              + result.getExitCode());
        }
        return false;
      }
    } catch (MavenInvocationException e) {
      e.printStackTrace();
      logger.warning("Failed to execute goal: " + e.getMessage());
      return false;
    }
    return true;
  }

  public static String executeGoals(String projectPath, String goals,
      Map<String, String> parameters) {
    InvocationOutputHandler outputHandler =
        new StringBufferInvocationOutputHandler();
    InvocationOutputHandler errorHandler =
        new StringBufferInvocationOutputHandler();
    if (execute(projectPath, goals, parameters, outputHandler, errorHandler)) {
      return outputHandler.toString();
    } else {
      return outputHandler.toString() + "\n" + errorHandler.toString();
    }
  }

  public static void executeGoals(String projectPath, String goals,
      Map<String, String> parameters, OutputStream outputStream) {
    InvocationOutputHandler outputHandler =
        new StreamingInvocationOutputHandler(outputStream);
    InvocationOutputHandler errorHandler =
        new StringBufferInvocationOutputHandler();
    if (!execute(projectPath, goals, parameters, outputHandler, errorHandler)) {
      outputHandler.consumeLine(errorHandler.toString());
    }
  }

  public static boolean execute(String projectPath, String goals,
      Map<String, String> parameters, InvocationOutputHandler outputHandler,
      InvocationOutputHandler errorHandler) {
    logger.info("Start to execute goal " + goals + " at " + projectPath);
    InvocationRequest request = new DefaultInvocationRequest();
    request.setBaseDirectory(new File(projectPath));
    request.setShowErrors(false);
    // request.setPomFile(new File(projectPath + "/pom.xml"));
    request.setInteractive(false);
    request.setGoals(Arrays.asList(goals.split(" ")));
    request.setErrorHandler(errorHandler);
    request.setOutputHandler(outputHandler);
    if (parameters != null) {
      Properties properties = new Properties();
      properties.putAll(parameters);
      request.setProperties(properties);
    }
    try {
      InvocationResult result = getInvoker().execute(request);
      if (result.getExitCode() != 0) {
        if (result.getExecutionException() != null) {
          logger.warning("Failed to execute goal: "
              + result.getExecutionException());
          errorHandler.consumeLine(result.getExecutionException().getMessage());
        } else {
          logger.warning("Failed to execute goal with exit code: "
              + result.getExitCode());
          errorHandler.consumeLine("EXIT " + result.getExitCode());
        }
        return false;
      }
    } catch (MavenInvocationException e) {
      e.printStackTrace();
      logger.warning("Failed to execute goal: " + e.getMessage());
      errorHandler.consumeLine(e.getMessage());
      return false;
    }
    return true;
  }

  public static void main(String[] args) throws MavenInvocationException {
    MavenUtil.logger = Logger.getLogger(MavenUtil.class.getName());
    MavenUtil.config = new EnvironmentConfig();
    String result =
        executeGoals("/Users/xiangl/workspace/clidecspace/HelloMaven/",
            "compile", null);
    System.out.println("built: " + result);
    Map<String, String> parameters = new HashMap<String, String>();
    parameters.put("exec.mainClass", "HelloMaven");
    result =
        executeGoals("/Users/xiangl/workspace/clidecspace/HelloMaven/",
            "exec:java", parameters);
    System.out.println("built: " + result);
    result =
        executeGoals("/Users/xiangl/clidec/codebase/ws1/helloweb",
            "clean war:war", null);
    System.out.println("built: " + result);
    executeGoals("/Users/xiangl/clidec/codebase/ws1/helloweb", "clean war:war",
        null, System.out);
    System.out.println("built.");
  }
}
