package clidec.util;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class JavaUtil {

  @Inject
  private static Logger logger;

  /**
   * Parse java code given.
   */
  public static String parseFile(String code) {
    logger.info("Starting to parse.");

    ASTParser parser = ASTParser.newParser(AST.JLS8);
    parser.setKind(ASTParser.K_COMPILATION_UNIT);
    parser.setSource(code.toCharArray());
    parser.setResolveBindings(true);
    CompilationUnit cu = (CompilationUnit) parser.createAST(null);

    final StringBuilder json =
        new StringBuilder("{\"id\":\"").append(
            cu.getPackage().getName().getFullyQualifiedName()).append(
            "\",\"children\":[");
    cu.accept(new ASTVisitor() {
      boolean isFirstType = true;
      boolean isFirstMethod = true;

      @Override
      public void endVisit(TypeDeclaration node) {
        json.append("]}");
      }

      @Override
      public void endVisit(MethodDeclaration node) {
        json.append("\"}");
      }

      @Override
      public boolean visit(TypeDeclaration node) {
        if (isFirstType) {
          isFirstType = false;
        } else {
          json.append(",");
        }
        json.append("{\"id\":\"")
            .append(node.getName().getFullyQualifiedName())
            .append("\",\"children\":[");
        return true;
      }

      @Override
      public boolean visit(MethodDeclaration node) {
        if (isFirstMethod) {
          isFirstMethod = false;
        } else {
          json.append(",");
        }
        json.append("{\"id\":\"")
            .append(node.getName().getFullyQualifiedName());
        return true;
      }
    });
    json.append("]}");
    return json.toString();
  }

  public static void main(String[] args) {
    String javeCode =
        "public class A { int i = 9;  \n int j; \n"
            + " ArrayList<Integer> al =  ArrayList<Integer>();j=1000; \n"
            + "int getI(){ return i;} }";
    System.out.println(JavaUtil.parseFile(javeCode));
  }
}
