package clidec.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import javax.inject.Inject;

public class ShellUtil {

  @Inject
  private static Logger logger;

  public static String executeShell(String shellCommand, String path) {
    return executeShell(shellCommand, new String[0], path);
  }

  public static String executeShell(String shellCommand, String[] envp,
      String path) {
    try {
      logger.info("execute shell command: " + shellCommand + " at " + path);
      String msg = "";
      Process process =
          Runtime.getRuntime().exec(shellCommand, envp, new File(path));
      BufferedReader input =
          new BufferedReader(new InputStreamReader(process.getInputStream()));
      String line = null;
      while ((line = input.readLine()) != null) {
        msg += line + "\n";
      }
      BufferedReader error =
          new BufferedReader(new InputStreamReader(process.getErrorStream()));
      while ((line = error.readLine()) != null) {
        msg += "[Error]: " + line + "\n";
      }
      int exitValue = process.waitFor();
      if (exitValue != 0) {
        msg += "Exited " + exitValue + "\n";
      }
      return msg;
    } catch (Exception e) {
      e.printStackTrace();
      logger.severe(e.getMessage());
      return e.getMessage();
    }
  }

  public static void main(String[] args) {
    ShellUtil.logger = Logger.getLogger(ShellUtil.class.getName());
    System.out.println(ShellUtil.executeShell("grep hello hello.tt",
        "/Users/xiangl/Documents"));
  }

}
