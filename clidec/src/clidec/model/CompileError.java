package clidec.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CompileError {
  private String path;
  private int line;
  private int start;
  private int end;
  private int severity;
  private String msg;

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public int getLine() {
    return line;
  }

  public void setLine(int line) {
    this.line = line;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getEnd() {
    return end;
  }

  public void setEnd(int end) {
    this.end = end;
  }

  public int getSeverity() {
    return severity;
  }

  public void setSeverity(int severity) {
    this.severity = severity;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  @Override
  public String toString() {
    return new StringBuffer("{line:").append(this.line).append(",start:")
        .append(this.start).append(",end:'").append(this.end)
        .append(",severity:'").append(this.severity).append(",msg:'")
        .append(this.msg).append("'}").toString();
  }
}
