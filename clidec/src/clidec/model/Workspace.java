package clidec.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Workspace {

  private String name;

  private long changeIndex;

  public Workspace() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getChangeIndex() {
    return changeIndex;
  }

  public void setChangeIndex(long changeIndex) {
    this.changeIndex = changeIndex;
  }

  @Override
  public String toString() {
    return new StringBuffer(" Name : ").append(this.name)
        .append(" Change Index : ").append(this.changeIndex).toString();
  }

}
