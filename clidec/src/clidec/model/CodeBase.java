package clidec.model;

import java.io.File;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

import clidec.EnvironmentConfig;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class CodeBase {
  @Inject
  private static Logger logger;

  @Inject
  private static EnvironmentConfig config;

  protected final ReadWriteLock lock = new ReentrantReadWriteLock();

  private String rootPath;

  public CodeBase() {
    rootPath =
        config.getUserHome() + File.separator + "clidec" + File.separator
            + "codebase";
    ensureRootPath();
  }

  private void ensureRootPath() {
    logger.info("code base path: " + rootPath);
    File codeBaseFolder = new File(rootPath);
    if (!codeBaseFolder.exists()) {
      logger.info("creating directory: " + rootPath);
      if (!codeBaseFolder.mkdirs()) {
        logger.severe("Failed to make directory for code base at " + rootPath);
      }
    }
  }

  public String getRoot() {
    return rootPath;
  }

  void setRoot(String root) {
    this.rootPath = root;
    ensureRootPath();
  }

  public static void main(String[] args) {
    CodeBase.logger = Logger.getLogger(CodeBase.class.getName());
    CodeBase codeBase = new CodeBase();

    System.out.println("user home: " + config.getUserHome());
    System.out.println("code base root: " + codeBase.getRoot());
  }
}
