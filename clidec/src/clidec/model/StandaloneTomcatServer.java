package clidec.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;

import clidec.EnvironmentConfig;

import com.google.inject.Singleton;

@Singleton
public class StandaloneTomcatServer {

  @Inject
  private static Logger logger;

  @Inject
  private static EnvironmentConfig config;

  private String basePath = config.getTomcatHome();
  private Set<String> deployedWebApps = new HashSet<>();
  private int port = 18080;
  private final ReadWriteLock lock = new ReentrantReadWriteLock();

  public StandaloneTomcatServer() {
    logger.info("CATANILA_HOME: " + basePath);
    this.init(this.port, this.basePath);
  }

  public void init(int port, String basePath) {
    this.port = port;
    this.basePath = basePath;
    if (!this.basePath.endsWith(File.separator)) {
      this.basePath += File.separator;
    }
    logger.info("configuring standalone tomcat server at " + basePath);
    deployedWebApps.clear();
  }

  private String executeShell(String shellCommand, String path) {
    try {
      logger.info("execute shell command: " + shellCommand + " at " + path);
      String msg = "";
      String[] envp = new String[] { "CATALINA_HOME=" + basePath };
      Process process =
          Runtime.getRuntime().exec(shellCommand, envp, new File(path));
      BufferedReader input =
          new BufferedReader(new InputStreamReader(process.getInputStream()));
      String line = null;
      while ((line = input.readLine()) != null) {
        msg += line + "\n";
      }
      BufferedReader error =
          new BufferedReader(new InputStreamReader(process.getErrorStream()));
      while ((line = error.readLine()) != null) {
        msg += line + "\n";
      }
      int exitValue = process.waitFor();
      if (exitValue != 0) {
        msg += "Exited " + exitValue + "\n";
      } else {
        msg += "SUCCESS\n";
      }
      return msg;
    } catch (Exception e) {
      return logException(e);
    }
  }

  public String run(String webAppName, String webAppWarPath,
      Map<String, String> parameters) {
    String msg = start();
    msg += "Deploy web app " + webAppName + " by war file.\n";
    msg += deploy(webAppName, webAppWarPath);
    return msg;
  }

  public String start() {
    lock.readLock().lock();
    try {
      String msg = executeShell("./startup.sh", basePath + "bin/");
      // executeShell("./run_tomcat.sh " + port + " &", basePath + "bin/");
      logger.info("tomcat dev server started at port " + port + ".\n");
      return msg;
    } finally {
      lock.readLock().unlock();
    }
  }

  public synchronized String deploy(String webAppName, String webAppWarPath) {
    lock.writeLock().lock();
    try {
      logger.info("Deploy web app " + webAppName + " by war file "
          + webAppWarPath);
      File warFile = new File(webAppWarPath);
      if (!warFile.exists()) {
        return logInfo("War file doesn't exist: " + webAppWarPath);
      }
      String webAppPath = basePath + "webapps/";
      logger.info("Deploy web app at " + webAppPath);
      String ownedWebAppPath = webAppPath + webAppName;
      File ownedWebAppDir = new File(ownedWebAppPath);
      if (ownedWebAppDir.exists()) {
        try {
          FileUtils.deleteDirectory(ownedWebAppDir);
        } catch (IOException e) {
          return logException(e);
        }
      }
      File ownedWarFile = new File(ownedWebAppPath + ".war");
      if (ownedWarFile.exists()) {
        if (!ownedWarFile.delete()) {
          logger.severe("Failed to remove file: " + ownedWebAppPath);
          return "Failed to remove war file for " + webAppName;
        }
      }
      try {
        FileUtils.copyFile(warFile, ownedWarFile);
      } catch (IOException e) {
        return logException(e);
      }
      deployedWebApps.add(webAppName);
      return logInfo("Successfully deployed war file for web app " + webAppName);
    } finally {
      lock.writeLock().unlock();
    }
  }

  public String stop() {
    lock.readLock().lock();
    try {
      String msg = executeShell("./shutdown.sh", basePath + "bin/");
      // executeShell("./run_tomcat.sh " + port + " &", basePath + "bin/");
      logger.info("tomcat dev server stopped!");
      return msg;
    } finally {
      lock.readLock().unlock();
    }
  }

  private String logInfo(String msg) {
    logger.info(msg);
    return msg;
  }

  private String logException(Exception e) {
    e.printStackTrace();
    logger.severe(e.getMessage());
    return e.getMessage();
  }

  public static void main(String[] args) throws InterruptedException {
    StandaloneTomcatServer.logger = Logger.getLogger(CodeBase.class.getName());
    StandaloneTomcatServer tomcatServer = new StandaloneTomcatServer();
    tomcatServer.init(28080, tomcatServer.basePath);
    String message =
        tomcatServer.run("clidee", "/Users/xiangl/tomcat-dev/clidee.war", null);
    System.out.println(message);
    Thread.sleep(15_000);
    message =
        tomcatServer
            .deploy("helloweb", "/Users/xiangl/tomcat-dev/helloweb.war");
    System.out.println(message);
    Thread.sleep(15_000);
    message = tomcatServer.stop();
    System.out.println(message);
  }
}
