package clidec.model;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.StoredConfig;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;

@Singleton
public class GitBase {

  @Inject
  private static Logger logger;

  @Inject
  private static CodeBase codeBase;

  private static final String GIT_PARAMETER_URL = "url";
  private static final String GIT_PARAMETER_PATH = "path";

  private Map<String, GitRepository> gitRepositories = new HashMap<>();
  private final ReadWriteLock lock = new ReentrantReadWriteLock();

  public GitBase() {
    initGitRepositories();
  }

  public void initGitRepositories() {
    gitRepositories.clear();
    File baseFolder = new File(codeBase.getRoot());
    for (File file : baseFolder.listFiles()) {
      if (file.isDirectory() && !file.getName().startsWith(".")) {
        initGitRepositories(file, file.getName());
      }
    }
  }

  private void initGitRepositories(File folder, String relativePath) {
    for (File file : folder.listFiles()) {
      if (file.isDirectory()) {
        if (file.getName().equals(".git")) {
          try {
            GitRepository gitRepository = new GitRepository(relativePath);
            gitRepositories.put(relativePath, gitRepository);
          } catch (IOException | GitAPIException e) {
            e.printStackTrace();
            logger.severe("Failed to init git repository at " + relativePath);
          }
        } else if (!file.getName().startsWith(".")) {
          initGitRepositories(file,
              relativePath + File.separator + file.getName());
        }
      }
    }
  }

  public String cloneRemoteRepository(String workspacePath,
      Map<String, String> parameters, Writer writer) {
    lock.writeLock().lock();
    try {
      String remoteUri = parameters.get(GIT_PARAMETER_URL);
      logger.info("make git repository in " + workspacePath
          + " by cloning remote repository from " + remoteUri);
      try {
        String localPath = workspacePath + parameters.get(GIT_PARAMETER_PATH);
        GitRepository gitRepository =
            new GitRepository(workspacePath, parameters, writer);
        gitRepositories.put(localPath, gitRepository);
        return "SUCCESS";
      } catch (GitAPIException | IOException e) {
        e.printStackTrace();
        return e.getMessage();
      }
    } finally {
      lock.writeLock().unlock();
    }
  }

  public String createRepository(String localPath) {
    lock.writeLock().lock();
    try {
      if (gitRepositories.containsKey(localPath)) {
        return "git repository has already created at " + localPath;
      }
      logger.info("make git repository at " + localPath);
      try {
        GitRepository gitRepository = new GitRepository(localPath);
        gitRepositories.put(localPath, gitRepository);
        return "SUCCESS";
      } catch (GitAPIException | IOException e) {
        e.printStackTrace();
        return e.getMessage();
      }
    } finally {
      lock.writeLock().unlock();
    }
  }

  public GitRepository getRepository(String localPath) {
    return gitRepositories.get(localPath);
  }

  public GitRepository findContainingRepository(String path) {
    String[] fileNames = path.split("/");
    if (fileNames.length == 0) {
      return null;
    }
    String folderPath = fileNames[0];
    for (int i = 1; i < fileNames.length; ++i) {
      folderPath += File.separator + fileNames[i];
      if (gitRepositories.containsKey(folderPath)) {
        return gitRepositories.get(folderPath);
      }
    }
    return null;
  }

  public Collection<GitRepository> getAllRepositories() {
    return gitRepositories.values();
  }

  public Collection<String> getAllRepositoryNames() {
    return gitRepositories.keySet();
  }

  public Collection<String> getRepositoryNames(String workspaceName) {
    Set<String> workspaceRepositories = new HashSet<String>();
    for (String fullRepositoryName : gitRepositories.keySet()) {
      if (fullRepositoryName.startsWith(workspaceName)) {
        String repositoryName =
            fullRepositoryName.substring(workspaceName.length() + 1);
        workspaceRepositories.add(repositoryName);
      }
    }
    return workspaceRepositories;
  }

  public static void main(String[] args) throws IOException {
    Injector injector = Guice.createInjector(new AbstractModule() {
      @Override
      protected void configure() {
        requestStaticInjection(CodeBase.class);
        requestStaticInjection(GitBase.class);
        requestStaticInjection(GitRepository.class);
        requestStaticInjection(Workspaces.class);
      }
    });
    Path tmpPath = Files.createTempDirectory("codebase");
    System.out.println("tmp path: " + tmpPath);
    try {
      Files.deleteIfExists(tmpPath);
    } catch (IOException e) {
      e.printStackTrace();
      System.out.println("Failed to remove folder: " + tmpPath);
      return;
    }
    codeBase.setRoot(tmpPath.toString());
    GitBase gitBase = injector.getInstance(GitBase.class);

    String remoteUri = "https://code.google.com/p/neuronet/";
    Map<String, String> parameters = new HashMap<String, String>();
    parameters.put(GIT_PARAMETER_URL, remoteUri);
    parameters.put(GIT_PARAMETER_PATH, "/neuronet");
    Writer writer = new PrintWriter(System.out);
    gitBase.cloneRemoteRepository("ws1", parameters, writer);
    System.out.println("git repositories "
        + gitBase.getAllRepositories().size());
    for (String repoName : gitBase.getAllRepositoryNames()) {
      System.out.println("git repository: " + repoName);
    }

    remoteUri = "gitolite3@clide.net:clide.git";
    Map<String, String> clideParameters = new HashMap<String, String>();
    clideParameters.put(GIT_PARAMETER_URL, remoteUri);
    clideParameters.put(GIT_PARAMETER_PATH, "/testing");
    clideParameters.put("username", "gitolite3");
    gitBase.cloneRemoteRepository("ws1", clideParameters, writer);
    System.out.println("git repositories "
        + gitBase.getAllRepositories().size());
    for (String repoName : gitBase.getAllRepositoryNames()) {
      System.out.println("git repository: " + repoName);
    }

    gitBase.createRepository("ws1");
    System.out.println("git repositories "
        + gitBase.getAllRepositoryNames().size());
    for (String repoName : gitBase.getAllRepositoryNames()) {
      System.out.println("git repository: " + repoName);
    }
    for (GitRepository gitRepository : gitBase.getAllRepositories()) {
      System.out.println("git repository: "
          + gitRepository.getRepository().getDirectory().getAbsolutePath());
    }

    Workspaces workspaces = injector.getInstance(Workspaces.class);
    Workspace workspace = new Workspace();
    workspace.setName("ws1");
    workspace.setChangeIndex(0);
    workspaces.addWorkspace(workspace);
    workspace = workspaces.getWorkspace("ws1");
    workspaces.addFolder("ws1", "p1");
    workspaces.writeFile("ws1", "p1/test.txt", "Test file content.");

    GitRepository gitRepository = gitBase.getRepository("ws1");
    Map<String, String> addParameters = new HashMap<String, String>();
    addParameters.put("default", "p1/test.txt");
    gitRepository.executeCommand("add", addParameters);

    Map<String, String> commitParameters = new HashMap<String, String>();
    commitParameters.put("default", "Temp ommit for testing.");
    gitRepository.executeCommand("commit", commitParameters);

    System.out.println(gitRepository.readFileAtHeadCommit("ws1/p1/test.txt"));

    remoteUri = "https://lx_codev@bitbucket.org/lx_codev/test-git.git";
    Map<String, String> cloneParameters = new HashMap<String, String>();
    cloneParameters.put("remote name", "test-git");
    cloneParameters.put(GIT_PARAMETER_URL, remoteUri);
    cloneParameters.put(GIT_PARAMETER_PATH, "/test");
    cloneParameters.put("username", "lx_codev");
    cloneParameters.put("password", "lingdang");
    Writer cloneWriter = new PrintWriter(System.out);
    gitBase.cloneRemoteRepository("test-git", cloneParameters, cloneWriter);
    System.out.println("git repositories "
        + gitBase.getAllRepositories().size());
    for (String repoName : gitBase.getAllRepositoryNames()) {
      System.out.println("git repository: " + repoName);
    }

    Workspace testGitWorkspace = new Workspace();
    testGitWorkspace.setName("test-git");
    testGitWorkspace.setChangeIndex(0);
    workspaces.addWorkspace(testGitWorkspace);
    testGitWorkspace = workspaces.getWorkspace("test-git");
    workspaces.addFolder("test-git", "test");
    workspaces.writeFile("test-git", "test/test.txt", "Test file content.");

    GitRepository gitPushRepository = gitBase.getRepository("test-git/test");
    StoredConfig config = gitPushRepository.getRepository().getConfig();
    for (String section : config.getSections()) {
      for (String name : config.getNames(section)) {
        System.out.println("section: " + section + ", name: " + name
            + ", value: " + config.getString(section, null, name));
      }
    }

    Map<String, String> addParameters2 = new HashMap<String, String>();
    addParameters2.put("default", "test/test.txt");
    gitPushRepository.executeCommand("add", addParameters2);

    Map<String, String> commitParameters2 = new HashMap<String, String>();
    commitParameters2.put("default", "Temp ommit for testing.");
    gitPushRepository.executeCommand("commit", commitParameters2);

    // System.out.println(gitRepository.readFileAtHeadCommit("test-git/test/test.txt"));

    Map<String, String> pushParameters = new HashMap<String, String>();
    pushParameters.put("remote name", "test-git");
    pushParameters.put("branch name", "master");
    pushParameters.put("username", "lx_codev");
    pushParameters.put("password", "lingdang");
    gitPushRepository.executeCommand("push", pushParameters);
  }
}
