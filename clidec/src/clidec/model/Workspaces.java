package clidec.model;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;

@Singleton
public class Workspaces {

  @Inject
  private static Logger logger;

  @Inject
  private static CodeBase codeBase;

  private Map<String, Workspace> workspaces = new HashMap<>();
  private final ReadWriteLock lock = new ReentrantReadWriteLock();

  public Workspaces() {
    File rootFolder = new File(codeBase.getRoot());
    File[] files = rootFolder.listFiles();
    for (File file : files) {
      if (file.isDirectory()) {
        Workspace workspace = new Workspace();
        workspace.setName(file.getName());
        workspaces.put(file.getName(), workspace);
      }
    }
  }

  public List<String> getAllWorkSpaces() {
    lock.readLock().lock();
    try {
      return new ArrayList<String>(workspaces.keySet());
    } finally {
      lock.readLock().unlock();
    }
  }

  public class FilePath {
    private String workspace;
    private String projectName;
    private String relativePath;
    private boolean isDirectory;
    private boolean isFile;

    FilePath(String workspace, String path) throws IOException {
      if (!workspaces.containsKey(workspace)) {
        throw new IOException("Workspace is not existing: " + workspace);
      }
      this.workspace = workspace;
      String workspacePath = codeBase.getRoot() + File.separator + workspace;
      File workspaceDir = new File(workspacePath);
      if (!workspaceDir.exists()) {
        throw new IOException("Workspace is not existing: " + workspace);
      }
      if (path == null) {
        return;
      }
      String[] files = path.split("/");
      if (files.length == 0) {
        throw new IOException("Project relative path doesn't exist: " + path);
      }
      this.projectName = files[0];
      File projectDir = new File(getProjectAbsolutePath());
      if (!projectDir.exists()) {
        throw new IOException("Project path doesn't exist: " + projectName);
      }
      if (files.length == 1) {
        this.relativePath = "";
      } else {
        this.relativePath = path.substring(projectName.length() + 1);
        File file = new File(getFileAbsolutePath());
        if (!file.exists()) {
          throw new IOException("File doesn't exist: " + path);
        }
        this.isDirectory = file.isDirectory();
        this.isFile = file.isFile();
      }
    }

    public String getWorkspaceName() {
      return workspace;
    }

    public String getWorkspaceAbsolutePath() {
      return codeBase.getRoot() + File.separator + workspace;
    }

    public boolean isProject() {
      return "".equals(this.relativePath);
    }

    public String getProjectName() {
      return projectName;
    }

    public String getProjectAbsolutePath() {
      return codeBase.getRoot() + File.separator + workspace + File.separator
          + projectName;
    }

    public String getFileWorkspaceRelativePath() {
      return projectName + File.separator + relativePath;
    }

    public String getFileProjectRelativePath() {
      return relativePath;
    }

    public String getFileAbsolutePath() {
      return codeBase.getRoot() + File.separator + workspace + File.separator
          + projectName + File.separator + relativePath;
    }

    public boolean isDirectory() {
      return this.isDirectory;
    }

    public boolean isFile() {
      return this.isFile;
    }
  }

  public Workspace getWorkspace(String workspace) {
    lock.readLock().lock();
    try {
      try {
        return workspaces.get(new FilePath(workspace, null).getWorkspaceName());
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return null;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public FilePath getFilePath(String workspaceName, String path) {
    lock.readLock().lock();
    try {
      try {
        return new FilePath(workspaceName, path);
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return null;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public String getProjectName(String workspaceName, String path) {
    lock.readLock().lock();
    try {
      try {
        return new FilePath(workspaceName, path).getProjectName();
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return null;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public String getProjectPath(String workspaceName, String path) {
    lock.readLock().lock();
    try {
      try {
        return new FilePath(workspaceName, path).getProjectAbsolutePath();
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return null;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public boolean isProject(String workspaceName, String path) {
    lock.readLock().lock();
    try {
      try {
        return new FilePath(workspaceName, path).isProject();
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return false;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public boolean isProjectDirectory(String workspaceName, String path) {
    lock.readLock().lock();
    try {
      try {
        return new FilePath(workspaceName, path).isDirectory();
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return false;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public boolean isProjectFile(String workspaceName, String path) {
    lock.readLock().lock();
    try {
      try {
        return new FilePath(workspaceName, path).isFile();
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return false;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public String getFileAbsolutePath(String workspaceName, String path) {
    lock.readLock().lock();
    try {
      try {
        return new FilePath(workspaceName, path).getFileAbsolutePath();
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return null;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public String getFileProjectRelativePath(String workspaceName, String path) {
    lock.readLock().lock();
    try {
      try {
        return new FilePath(workspaceName, path).getFileProjectRelativePath();
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return null;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public String findLatestWarFile(String workspaceName, String projectPath) {
    lock.readLock().lock();
    try {
      try {
        FilePath filePath = new FilePath(workspaceName, projectPath);
        if (!filePath.isProject()) {
          logger.warning("The target path is not a project: " + projectPath);
          return null;
        }
        String targetPath =
            filePath.getProjectAbsolutePath() + File.separator + "target";
        File targetDir = new File(targetPath);
        File[] files = targetDir.listFiles(new FileFilter() {
          public boolean accept(File file) {
            return file.isFile() && file.getName().endsWith(".war");
          }
        });
        long lastModified = Long.MIN_VALUE;
        File latestWar = null;
        for (File file : files) {
          if (file.lastModified() > lastModified) {
            latestWar = file;
            lastModified = file.lastModified();
          }
        }
        if (latestWar == null) {
          return null;
        }
        return targetPath + File.separator + latestWar.getName();
      } catch (IOException e) {
        logger.warning(e.getMessage());
        return null;
      }
    } finally {
      lock.readLock().unlock();
    }
  }

  public Workspace addWorkspace(Workspace workspace) {
    lock.writeLock().lock();
    try {
      logger.info("Add workspace: " + workspace.getName());
      String workspacePath =
          codeBase.getRoot() + File.separator + workspace.getName();
      File workspaceDir = new File(workspacePath);
      if (workspaceDir.exists()) {
        logger.warning("Workspace is existing: " + workspace.getName());
        return null;
      }
      logger.info("Creating workspace: " + workspace.getName());
      if (!workspaceDir.mkdir()) {
        logger.severe("Failed to create workspace: " + workspace.getName());
        return null;
      }
      return workspaces.put(workspace.getName(), workspace);
    } finally {
      lock.writeLock().unlock();
    }
  }

  public Workspace removeWorkspace(String name) {
    lock.writeLock().lock();
    try {
      if (!workspaces.containsKey(name)) {
        logger.warning("Workspace is not existing: " + name);
        return null;
      }
      String workspacePath = codeBase.getRoot() + File.separator + name;
      File workspaceDir = new File(workspacePath);
      if (!workspaceDir.exists()) {
        logger.warning("Workspace dir is not existing: " + name);
        return null;
      }
      try {
        logger.info("Removing workspace: " + name);
        FileUtils.deleteDirectory(workspaceDir);
      } catch (IOException e) {
        e.printStackTrace();
        logger.severe("Failed to remove workspace: " + name);
        return null;
      }
      return workspaces.remove(name);
    } finally {
      lock.writeLock().unlock();
    }
  }

  public List<String> getWorkspaceFiles(String name, String path) {
    lock.readLock().lock();
    try {
      if (!workspaces.containsKey(name)) {
        logger.warning("Workspace is not existing: " + name);
        return null;
      }
      String dirPath =
          codeBase.getRoot() + File.separator + name + File.separator + path;
      File dir = new File(dirPath);
      if (!dir.exists()) {
        logger.warning("Queryed path is not existing: " + dirPath);
        return null;
      }
      return Arrays.asList(dir.list());
    } finally {
      lock.readLock().unlock();
    }
  }

  private String toJsonTree(File file) {
    Comparator<File> comparator = new Comparator<File>() {
      public int compare(File f1, File f2) {
        if (f1.isDirectory() && !f2.isDirectory()) {
          return -1; // Directory before non-directory
        } else if (!f1.isDirectory() && f2.isDirectory()) {
          return 1; // Non-directory after directory
        } else {
          return f1.compareTo(f2); // Alphabetic order otherwise
        }
      }
    };
    if (file.isFile()) {
      return "{\"id\":\"" + file.getName() + "\"}";
    } else {
      String result = "{";
      result += "\"id\":\"" + file.getName() + "\",";
      result += "\"children\":[";
      File[] files = file.listFiles();
      Arrays.sort(files, comparator);
      if (files.length > 0) {
        result += toJsonTree(files[0]);
      }
      for (int i = 1; i < files.length; ++i) {
        result += "," + toJsonTree(files[i]);
      }
      result += "]}";
      return result;
    }
  }

  public String getWorkspaceFileTree(String name) {
    lock.readLock().lock();
    try {
      if (!workspaces.containsKey(name)) {
        logger.warning("Workspace is not existing: " + name);
        return null;
      }
      String dirPath =
          codeBase.getRoot() + File.separator + name + File.separator;
      File dir = new File(dirPath);
      if (!dir.exists()) {
        logger.warning("Queryed path is not existing: " + dirPath);
        return null;
      }
      return toJsonTree(dir);
    } finally {
      lock.readLock().unlock();
    }
  }

  public boolean addFolder(String name, String path) {
    lock.writeLock().lock();
    try {
      logger.info("Add folder: " + path + " in workspace " + name);
      if (!workspaces.containsKey(name)) {
        logger.warning("Workspace is not existing: " + name);
        return false;
      }
      String dirPath =
          codeBase.getRoot() + File.separator + name + File.separator + path;
      File dir = new File(dirPath);
      if (dir.exists()) {
        logger.warning("Folder is existing: " + dirPath);
        return false;
      }
      logger.info("creating folder: " + dirPath);
      if (!dir.mkdir()) {
        logger.severe("Failed to create folder: " + dirPath);
        return false;
      }
      return true;
    } finally {
      lock.writeLock().unlock();
    }
  }

  public boolean removeFolder(String name, String path) {
    lock.writeLock().lock();
    try {
      logger.info("Remove folder: " + path + " in workspace " + name);
      if (!workspaces.containsKey(name)) {
        logger.warning("Workspace is not existing: " + name);
        return false;
      }
      String dirPath =
          codeBase.getRoot() + File.separator + name + File.separator + path;
      File dir = new File(dirPath);
      if (!dir.exists()) {
        logger.warning("Folder is not existing: " + dirPath);
        return false;
      }
      try {
        logger.info("Removing folder: " + dirPath);
        FileUtils.deleteDirectory(dir);
      } catch (IOException e) {
        e.printStackTrace();
        logger.severe("Failed to remove folder: " + dirPath);
        return false;
      }
      return true;
    } finally {
      lock.writeLock().unlock();
    }
  }

  public boolean addFile(String name, String path) {
    lock.writeLock().lock();
    try {
      logger.info("Add file: " + path + " in workspace " + name);
      if (!workspaces.containsKey(name)) {
        logger.warning("Workspace is not existing: " + name);
        return false;
      }
      String filePath =
          codeBase.getRoot() + File.separator + name + File.separator + path;
      File file = new File(filePath);
      File dir = file.getParentFile();
      if (!dir.exists()) {
        logger.warning("The parent folder is not existing: " + filePath);
        return false;
      }
      if (file.exists()) {
        logger.warning("The file is existing: " + filePath);
        return false;
      }
      try {
        logger.info("Creating file: " + filePath);
        if (!file.createNewFile()) {
          logger.severe("Failed to create file: " + filePath);
          return false;
        }
      } catch (IOException e) {
        e.printStackTrace();
        logger.severe("Failed to create file: " + filePath);
        return false;
      }
      return true;
    } finally {
      lock.writeLock().unlock();
    }
  }

  public boolean removeFile(String name, String path) {
    lock.writeLock().lock();
    try {
      logger.info("Remove file: " + path + " in workspace " + name);
      if (!workspaces.containsKey(name)) {
        logger.warning("Workspace is not existing: " + name);
        return false;
      }
      String filePath =
          codeBase.getRoot() + File.separator + name + File.separator + path;
      File file = new File(filePath);
      if (!file.exists()) {
        logger.warning("File is not existing: " + filePath);
        return false;
      }
      logger.info("Removing file: " + filePath);
      if (!file.delete()) {
        logger.severe("Failed to remove file: " + filePath);
        return false;
      }
      return true;
    } finally {
      lock.writeLock().unlock();
    }
  }

  public String readFile(String name, String path) {
    lock.readLock().lock();
    try {
      if (!workspaces.containsKey(name)) {
        logger.warning("Workspace is not existing: " + name);
        return null;
      }
      String filePath =
          codeBase.getRoot() + File.separator + name + File.separator + path;
      try {
        logger.info("Reading file: " + filePath);
        byte[] encoded = Files.readAllBytes(Paths.get(filePath));
        return new String(encoded, Charsets.UTF_8);
      } catch (IOException e) {
        e.printStackTrace();
        logger.severe("Failed to read file: " + filePath);
      }
      return null;
    } finally {
      lock.readLock().unlock();
    }
  }

  public String writeFile(String name, String path, String content) {
    lock.readLock().lock();
    try {
      if (!workspaces.containsKey(name)) {
        logger.warning("Workspace is not existing: " + name);
        return null;
      }
      String filePath =
          codeBase.getRoot() + File.separator + name + File.separator + path;
      logger.info("Writing file: " + filePath);
      try (PrintWriter writer = new PrintWriter(filePath)) {
        writer.print(content);
        return content;
      } catch (IOException e) {
        e.printStackTrace();
        logger.severe("Failed to write file: " + filePath);
      }
      return null;
    } finally {
      lock.readLock().unlock();
    }
  }

  public static void main(String[] args) throws Exception {
    Injector injector = Guice.createInjector(new AbstractModule() {
      @Override
      protected void configure() {
        requestStaticInjection(CodeBase.class);
        requestStaticInjection(Workspaces.class);
      }
    });
    File tmpPath = File.createTempFile("codebase", "");
    System.out.println("tmp path: " + tmpPath.getAbsolutePath());
    tmpPath.delete();
    codeBase.setRoot(tmpPath.getAbsolutePath());
    Workspaces workspaces = injector.getInstance(Workspaces.class);

    Workspace workspace = new Workspace();
    workspace.setName("test");
    workspace.setChangeIndex(0);
    workspaces.addWorkspace(workspace);
    workspace = workspaces.getWorkspace("test");
    System.out.println("test workspace: " + workspace.getName());

    workspaces.addFolder("test", "dir");
    workspaces.addFolder("test", "dir2");
    workspaces.addFolder("test", "dir/subdir");
    workspaces.addFolder("test", "dir/subdir1");
    String dirPath = codeBase.getRoot() + "/test/dir";
    PrintWriter writer = new PrintWriter(dirPath + "/subdir/test.txt", "UTF-8");
    writer.println("The first line");
    writer.println("The second line");
    writer.close();
    workspaces.addFile("test", "dir/subdir1/test.txt");
    workspaces.addFile("test", "dir/subdir1/test2.txt");
    System.out.println("file tree: " + workspaces.getWorkspaceFileTree("test"));
    workspaces.removeFile("test", "dir/subdir1/test.txt");
    workspaces.removeFolder("test", "dir/subdir1");
    workspaces.removeFolder("test", "dir");

    System.out.println("workspaces " + workspaces.getAllWorkSpaces().size());
    for (String ws : workspaces.getAllWorkSpaces()) {
      System.out.println("workspace: " + ws);
    }
  }
}
