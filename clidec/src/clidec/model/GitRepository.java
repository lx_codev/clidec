package clidec.model;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.errors.NoWorkTreeException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ProgressMonitor;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.lib.TextProgressMonitor;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.FetchResult;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.util.StringUtils;

import com.google.inject.Inject;

public class GitRepository {

  @Inject
  private static Logger logger;

  @Inject
  private static CodeBase codeBase;

  private static final String GIT_PARAMETER_DEFAULT = "default";
  private static final String GIT_PARAMETER_URL = "url";
  private static final String GIT_PARAMETER_USERNAME = "username";
  private static final String GIT_PARAMETER_PASSWORD = "password";
  private static final String GIT_PARAMETER_PATH = "path";
  private static final String GIT_PARAMETER_REMOTE_NAME = "remote name";

  private final ReadWriteLock lock = new ReentrantReadWriteLock();

  private String localPath;
  private Repository localRepo;
  private Git git;

  public GitRepository(String workspacePath, Map<String, String> parameters,
      Writer writer) throws IOException, GitAPIException {
    String remoteName = parameters.get(GIT_PARAMETER_REMOTE_NAME);
    String remoteUri = parameters.get(GIT_PARAMETER_URL);
    String username = parameters.get(GIT_PARAMETER_USERNAME);
    String password = parameters.get(GIT_PARAMETER_PASSWORD);
    String localPath = workspacePath + parameters.get(GIT_PARAMETER_PATH);
    logger.info("Clone remote repository " + remoteUri + " to " + localPath);
    this.localPath = localPath;
    File repoFolder = new File(codeBase.getRoot() + File.separator + localPath);

    CloneCommand clone =
        Git.cloneRepository().setCredentialsProvider(
            new GitCredentialsProvider(username, password));
    ProgressMonitor monitor = new TextProgressMonitor(writer);
    git =
        clone.setURI(remoteUri).setDirectory(repoFolder)
            .setProgressMonitor(monitor).call();
    localRepo = FileRepositoryBuilder.create(repoFolder);

    StoredConfig config = git.getRepository().getConfig();
    config.setString("remote", remoteName, GIT_PARAMETER_URL, remoteUri);
    if (username != null) {
      config.setString("remote", remoteName, GIT_PARAMETER_USERNAME, username);
    }
    if (password != null) {
      config.setString("remote", remoteName, GIT_PARAMETER_PASSWORD, password);
    }
    config.save();
  }

  public GitRepository(String localPath) throws IOException, GitAPIException {
    this.localPath = localPath;
    String repoPath = codeBase.getRoot() + File.separator + localPath;
    File repoFolder = new File(repoPath, ".git");
    if (repoFolder.exists()) {
      localRepo = FileRepositoryBuilder.create(repoFolder);
      git = new Git(localRepo);
    } else {
      git = Git.init().setDirectory(new File(repoPath)).call();
      localRepo = FileRepositoryBuilder.create(repoFolder);
    }
  }

  public String getLocalPath() {
    return this.localPath;
  }

  public Repository getRepository() {
    return this.localRepo;
  }

  private void extractNonEmptyStatusMessage(StringBuilder msg, String status,
      Set<String> files) {
    if (!files.isEmpty()) {
      msg.append(status).append(":\n");
      msg.append("    ").append(StringUtils.join(files, "\n    ")).append("\n");
    }
  }

  public String readFileAtHeadCommit(String path) {
    if (!path.startsWith(this.localPath + File.separator)) {
      logger.warning("File path is not in local repository: " + path);
      return "";
    }
    path = path.substring(this.localPath.length() + 1);

    try {
      // Find the HEAD.
      ObjectId lastCommitId = localRepo.resolve(Constants.HEAD);
      // Get the commit object for that revision.
      RevWalk revWalk = new RevWalk(localRepo);
      RevCommit commit = revWalk.parseCommit(lastCommitId);
      // Get the revision's file tree.
      RevTree tree = commit.getTree();
      // Narrow it down to the single file's path.
      TreeWalk treewalk = TreeWalk.forPath(localRepo, path, tree);
      if (treewalk != null) {
        // use the blob id to read the file's data
        byte[] data = localRepo.open(treewalk.getObjectId(0)).getBytes();
        return new String(data, "utf-8");
      }
    } catch (RevisionSyntaxException | IOException e) {
      e.printStackTrace();
    }
    logger.warning("Failed to read head at file " + path);
    return "";
  }

  public String executeCommand(String command, Map<String, String> parameters) {
    lock.writeLock().lock();
    try {
      logger.info("Execut git " + command + " at " + localPath);
      StringBuilder message = new StringBuilder();
      try {
        switch (command) {
        case "status":
          Status status = git.status().call();
          extractNonEmptyStatusMessage(message, "Added", status.getAdded());
          extractNonEmptyStatusMessage(message, "Changed", status.getChanged());
          extractNonEmptyStatusMessage(message, "Conflicting",
              status.getConflicting());
          extractNonEmptyStatusMessage(message, "ConflictingStageState", status
              .getConflictingStageState().keySet());
          extractNonEmptyStatusMessage(message, "Missing", status.getMissing());
          extractNonEmptyStatusMessage(message, "Modified",
              status.getModified());
          extractNonEmptyStatusMessage(message, "Removed", status.getRemoved());
          extractNonEmptyStatusMessage(message, "Untracked",
              status.getUntracked());
          extractNonEmptyStatusMessage(message, "UntrackedFolders",
              status.getUntrackedFolders());
          extractNonEmptyStatusMessage(message, "IgnoredNotInIndex",
              status.getIgnoredNotInIndex());
          break;
        case "add":
          String filePattern = parameters.get(GIT_PARAMETER_DEFAULT);
          git.add().addFilepattern(filePattern).call();
          message.append("Successfully added file " + filePattern);
          break;
        case "commit":
          String gitMessage = parameters.get(GIT_PARAMETER_DEFAULT);
          RevCommit revCommit = git.commit().setMessage(gitMessage).call();
          message.append(revCommit.getAuthorIdent().getName()).append(" [");
          message.append(revCommit.getAuthorIdent().getWhen()).append("]: ");
          message.append(revCommit.getFullMessage());
          message.append("Successfully commit change: " + revCommit.getName());
          break;
        case "log":
          try {
            for (RevCommit commit : git.log().call()) {
              message.append(commit.getAuthorIdent().getName()).append(" [");
              message.append(commit.getAuthorIdent().getWhen()).append("]: ");
              message.append(commit.getFullMessage() + "\n");
            }
          } catch (NoHeadException e) {
            message.append("Empty repo, nothing to log.");
          }
          break;
        case "remote_add": {
          String remoteName = parameters.get(GIT_PARAMETER_REMOTE_NAME);
          String url = parameters.get(GIT_PARAMETER_URL);
          String username = parameters.get(GIT_PARAMETER_USERNAME);
          String password = parameters.get(GIT_PARAMETER_PASSWORD);
          try {
            StoredConfig config = git.getRepository().getConfig();
            config.setString("remote", remoteName, GIT_PARAMETER_URL, url);
            if (username != null) {
              config.setString("remote", remoteName, GIT_PARAMETER_USERNAME,
                  username);
            }
            if (password != null) {
              config.setString("remote", remoteName, GIT_PARAMETER_PASSWORD,
                  password);
            }
            config.save();
            message.append("Successfully add remote " + remoteName + "!\n");
            message.append(config.getSubsections("remote"));
          } catch (IOException e) {
            message.append("Failed to add remote " + remoteName + " at url "
                + url);
          }
          break;
        }
        case "remote_remove": {
          try {
            String remoteName = parameters.get(GIT_PARAMETER_REMOTE_NAME);
            StoredConfig config = git.getRepository().getConfig();
            config.unsetSection("remote", remoteName);
            config.save();
            message.append("Successfully remove remote " + remoteName + "!\n");
            message.append(config.getSubsections("remote"));
          } catch (IOException e) {
            message.append("Failed to remove remote url.");
          }
          break;
        }
        case "push": {
          String remoteName = parameters.get(GIT_PARAMETER_REMOTE_NAME);
          StoredConfig config = git.getRepository().getConfig();
          String username =
              config.getString("remote", remoteName, GIT_PARAMETER_USERNAME);
          String password =
              config.getString("remote", remoteName, GIT_PARAMETER_PASSWORD);
          PushCommand pushCommand = git.push();
          pushCommand.setRemote(remoteName);
          pushCommand.setCredentialsProvider(new GitCredentialsProvider(
              username, password));
          Iterable<PushResult> pushResults = pushCommand.call();
          message.append("Successfully push to remote " + remoteName + "\n");
          for (PushResult pushResult : pushResults) {
            message.append(pushResult.getMessages());
          }
          break;
        }
        case "pull": {
          String remoteName = parameters.get(GIT_PARAMETER_REMOTE_NAME);
          StoredConfig config = git.getRepository().getConfig();
          String username =
              config.getString("remote", remoteName, GIT_PARAMETER_USERNAME);
          String password =
              config.getString("remote", remoteName, GIT_PARAMETER_PASSWORD);
          PullCommand pullCommand = git.pull();
          pullCommand.setRemote(remoteName);
          pullCommand.setCredentialsProvider(new GitCredentialsProvider(
              username, password));
          PullResult pullResult = pullCommand.call();
          message.append("Successfully pull from remote " + remoteName + "\n");
          FetchResult fetchResult = pullResult.getFetchResult();
          message.append(fetchResult.getMessages());
          MergeResult mergeResult = pullResult.getMergeResult();
          message.append(mergeResult.getMergeStatus());
          break;
        }
        default:
          message.append("Invalid git command ").append(command);
        }
      } catch (NoWorkTreeException | GitAPIException e) {
        e.printStackTrace();
        logger.severe("Failed to execut git " + command + " at " + localPath
            + ": " + e.getMessage());
        message.append(e.getMessage());
      }
      logger.info("Executed git " + command + " at " + localPath + ":\n"
          + message);
      return message.toString();
    } finally {
      lock.writeLock().unlock();
    }
  }

  private static class GitCredentialsProvider extends CredentialsProvider {
    private String username;
    private String password;

    GitCredentialsProvider(String username, String password) {
      this.username = username;
      this.password = password;
    }

    @Override
    public boolean supports(CredentialItem... items) {
      return true;
    }

    @Override
    public boolean isInteractive() {
      return true;
    }

    @Override
    public boolean get(URIish uri, CredentialItem... items)
        throws UnsupportedCredentialItem {
      for (CredentialItem item : items) {
        if (item instanceof CredentialItem.Username) {
          ((CredentialItem.Username) item).setValue(username);
          continue;
        }
        if (item instanceof CredentialItem.Password) {
          ((CredentialItem.Password) item).setValue(password.toCharArray());
          continue;
        }
        if (item instanceof CredentialItem.StringType) {
          if (item.isValueSecure()) {
            ((CredentialItem.StringType) item).setValue(password);
            continue;
          }
          ((CredentialItem.Username) item).setValue(username);
          continue;
        }
      }
      return true;
    }
  }
}
