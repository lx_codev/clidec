package clidec.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import clidec.model.GitBase;
import clidec.model.GitRepository;
import clidec.util.JsonUtil;

import com.google.inject.Inject;

@Path("/gt")
public class GitService {

  @Inject
  private static Logger logger;

  @Inject
  private GitBase gitBase;

  @GET
  @Path("/ls/{ws}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> listAllGitRepositories(
      @PathParam("ws") String workspaceName) {
    return new ArrayList<String>(gitBase.getRepositoryNames(workspaceName));
  }

  @GET
  @Path("/head/{path:.+}")
  @Produces(MediaType.TEXT_PLAIN)
  public String getFileContentAtHead(@PathParam("path") String path) {
    logger.info("request file content at head for file " + path);
    GitRepository gitRepo = gitBase.findContainingRepository(path);
    if (gitRepo != null) {
      return gitRepo.readFileAtHeadCommit(path);
    }
    return "";
  }

  @POST
  @Path("/{cmd}/{path:.+}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_PLAIN)
  public Response executeGitCommandWithParameter(
      final @PathParam("cmd") String gitCommand,
      final @PathParam("path") String path,
      final Map<String, String> jsonParameters) {
    logger.info("request git " + gitCommand + " at path " + path
        + " with parameter " + jsonParameters);
    final Map<String, String> paramters =
        JsonUtil.parseJsonParameters(jsonParameters);
    StreamingOutput stream = new StreamingOutput() {
      @Override
      public void write(OutputStream outputStream) throws IOException,
          WebApplicationException {
        Writer writer = new PrintWriter(outputStream);
        String message = "";
        switch (gitCommand) {
        case "clone":
          message = gitBase.cloneRemoteRepository(path, paramters, writer);
          break;
        case "init":
          message = gitBase.createRepository(path);
          break;
        case "add":
        case "commit":
        case "status":
        case "log":
        case "remote_add":
        case "remote_remove":
        case "push":
        case "pull":
          GitRepository gitRepo = gitBase.getRepository(path);
          message = gitRepo.executeCommand(gitCommand, paramters);
          break;
        default:
          message =
              "Invalid git command " + gitCommand + " or parameter "
                  + jsonParameters;
        }
        logger.info("response message " + message);
        writer.write(message);
        writer.flush();
      }
    };
    return Response.ok(stream).build();
  }
}
