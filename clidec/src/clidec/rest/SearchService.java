package clidec.rest;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import clidec.model.Workspaces;
import clidec.model.Workspaces.FilePath;
import clidec.util.ShellUtil;

import com.google.inject.Inject;

@Path("/sc")
public class SearchService {

  @Inject
  private static Logger logger;

  @Inject
  private Workspaces workspaces;

  @POST
  @Path("/{engine}/{ws}/{path:.+}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public String searchCode(@PathParam("engine") String engine,
      @PathParam("ws") String workspaceName, @PathParam("path") String path,
      String query) {
    logger.info("search " + path + " in " + workspaceName + " with query "
        + query);
    FilePath filePath = workspaces.getFilePath(workspaceName, path);
    if (engine.equals("grep")) {
      return ShellUtil.executeShell("grep " + query.replace('+', ' ') + " "
          + filePath.getFileProjectRelativePath(),
          filePath.getProjectAbsolutePath());
    } else {
      return "Unknow search engine: " + engine;
    }
  }
}
