package clidec.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/us")
public class UserService {

  @GET
  @Path("/{user}")
  @Produces(MediaType.TEXT_PLAIN)
  public String login(@PathParam("user") String user) {
    return "Welcome " + user + "!";
  }
}