package clidec.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import clidec.model.Workspace;
import clidec.model.Workspaces;

import com.google.inject.Inject;

@Path("/ws")
public class WorkspacesService {

  @Inject
  private static Logger logger;

  @Inject
  private Workspaces workspaces;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> getAllWorkspaces() {
    return new ArrayList<String>(workspaces.getAllWorkSpaces());
  }

  @POST
  @Path("/{op}/{ws}")
  @Produces(MediaType.APPLICATION_JSON)
  public Workspace operateWorkspace(@PathParam("op") String op,
      @PathParam("ws") String workspaceName) {
    logger.info("request to " + op + " workspace " + workspaceName);
    if (op.equals("get")) {
      Workspace workspace = workspaces.getWorkspace(workspaceName);
      return workspace;
    } else if (op.equals("add")) {
      Workspace workspace = new Workspace();
      workspace.setName(workspaceName);
      workspaces.addWorkspace(workspace);
      return workspace;
    } else if (op.equals("remove")) {
      Workspace workspace = new Workspace();
      workspace.setName(workspaceName);
      return workspaces.removeWorkspace(workspaceName);
    }
    return null;
  }
}