package clidec.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import clidec.model.Workspaces;
import clidec.util.JsonUtil;
import clidec.util.MavenUtil;

import com.google.inject.Inject;

@Path("/mv")
public class MavenService {

  @Inject
  private static Logger logger;

  @Inject
  private Workspaces workspaces;

  @GET
  @Path("/{goals}/{ws}/{project}")
  @Produces(MediaType.TEXT_PLAIN)
  public String executeMavenGoals(@PathParam("goals") String goals,
      @PathParam("ws") String workspaceName,
      @PathParam("project") String projectName) {
    logger.info("request " + goals + " for workspace " + workspaceName
        + " in project " + projectName);
    String projectPath = workspaces.getProjectPath(workspaceName, projectName);
    return MavenUtil.executeGoals(projectPath, goals.replace('+', ' '), null);
  }

  @POST
  @Path("/{goals}/{ws}/{project}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_PLAIN)
  public Response executeMavenGoalsWithParameter(
      @PathParam("goals") final String goals,
      @PathParam("ws") String workspaceName,
      @PathParam("project") String projectName,
      Map<String, String> jsonParameters) {
    logger.info("request " + goals + " with parameters " + jsonParameters
        + " for workspace " + workspaceName + " in project " + projectName);
    final String projectPath =
        workspaces.getProjectPath(workspaceName, projectName);
    final Map<String, String> parameters =
        JsonUtil.parseJsonParameters(jsonParameters);
    StreamingOutput stream = new StreamingOutput() {
      @Override
      public void write(OutputStream outputStream) throws IOException,
          WebApplicationException {
        MavenUtil.executeGoals(projectPath, goals.replace('+', ' '),
            parameters, outputStream);
      }
    };
    return Response.ok(stream).build();
  }

}
