package clidec.rest;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import clidec.model.Workspaces;
import clidec.util.JavaUtil;

import com.google.inject.Inject;

@Path("/js")
public class JavaService {

  @Inject
  private static Logger logger;

  @Inject
  private Workspaces workspaces;

  @GET
  @Path("/parse/{ws}/{path:.+}")
  @Produces(MediaType.TEXT_PLAIN)
  public String getFileContent(@PathParam("ws") String workspaceName,
      @PathParam("path") String path) {
    logger
        .info("parse java file at " + path + " in workspace " + workspaceName);
    String code = workspaces.readFile(workspaceName, path);
    return JavaUtil.parseFile(code);
  }

  @POST
  @Path("/ast/{ws}/{path:.+}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public String writeFile(@PathParam("ws") String workspaceName,
      @PathParam("path") String path, String content) {
    logger.info("parse java file in workspace " + workspaceName + " at " + path
        + " with content:\n" + content);
    if (!workspaces.isProjectFile(workspaceName, path)) {
      logger.warning("File doesn't exist in workspace " + workspaceName
          + " at " + path);
      return "{}";
    }
    return JavaUtil.parseFile(content);
  }
}