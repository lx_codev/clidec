package clidec.rest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.maven.shared.invoker.InvocationOutputHandler;

import clidec.model.CompileError;
import clidec.model.GitBase;
import clidec.model.Workspaces;
import clidec.util.MavenUtil;
import clidec.util.MavenUtil.StringBufferInvocationOutputHandler;

import com.google.inject.Inject;

@Path("/fs")
public class FileService {

  private static final Pattern pattern = Pattern
      .compile("\\[(\\d+),(\\d+)\\] (.*)");

  @Inject
  private static Logger logger;

  @Inject
  private Workspaces workspaces;

  @Inject
  private GitBase gitBase;

  @GET
  @Path("/{ws}")
  @Produces(MediaType.TEXT_PLAIN)
  public String getWorkspaceFiles(@PathParam("ws") String workspaceName) {
    return workspaces.getWorkspaceFileTree(workspaceName);
  }

  @GET
  @Path("/read_file/{ws}/{path:.+}")
  @Produces(MediaType.TEXT_PLAIN)
  public String getFileContent(@PathParam("ws") String workspaceName,
      @PathParam("path") String path) {
    logger.info("request file content for file " + path + " in workspace "
        + workspaceName);
    return workspaces.readFile(workspaceName, path);
  }

  @GET
  @Path("/compile_file/{ws}/{path:.+}")
  @Produces(MediaType.APPLICATION_JSON)
  public CompileError[] compileFile(@PathParam("ws") String workspaceName,
      @PathParam("path") String path) {
    logger.info("request compilation for file " + path + " in workspace "
        + workspaceName);
    String projectPath = workspaces.getProjectPath(workspaceName, path);
    final String filePath = workspaces.getFileAbsolutePath(workspaceName, path);
    final String errorPrefix = "[ERROR] " + filePath + ":";
    final Set<String> errorLines = new HashSet<>();
    InvocationOutputHandler outputHandler = new InvocationOutputHandler() {
      public void consumeLine(String outputLine) {
        if (outputLine.startsWith(errorPrefix)) {
          errorLines.add(outputLine);
        }
      }
    };
    InvocationOutputHandler errorHandler =
        new StringBufferInvocationOutputHandler();
    if (!MavenUtil.execute(projectPath, "compile", null, outputHandler,
        errorHandler)) {
      logger.warning("Failed to compile file " + path);
    }
    final List<CompileError> compileErrors = new ArrayList<>();
    final String[] codeLines = workspaces.readFile(workspaceName, path).split("\n");
    for (String errorLine : errorLines) {
      System.out.println("error line: " + errorLine);
      String errorInfo = errorLine.substring(errorPrefix.length());
      Matcher matcher = pattern.matcher(errorInfo);
      if (matcher.find()) {
        int line = Integer.parseInt(matcher.group(1));
        int start = Integer.parseInt(matcher.group(2));
        String msg = matcher.group(3);
        String codeLine = codeLines[line - 1];
        System.out.println("code line: " + codeLine);
        int end = codeLine.substring(start).indexOf(' ');
        CompileError compileError = new CompileError();
        compileError.setLine(line);
        compileError.setStart(start);
        compileError.setEnd(start + end);
        compileError.setSeverity(0);
        compileError.setMsg(msg);
        compileErrors.add(compileError);
      }
    }
    return compileErrors.toArray(new CompileError[compileErrors.size()]);
  }

  @GET
  @Path("/{op}/{ws}/{path:.+}")
  @Produces(MediaType.APPLICATION_JSON)
  public String operateWorkspaceFiles(@PathParam("op") String op,
      @PathParam("ws") String workspaceName, @PathParam("path") String path) {
    logger.info("request " + op + " for workspace " + workspaceName + " at "
        + path);
    boolean status = false;
    if (op.equals("add_folder")) {
      status = workspaces.addFolder(workspaceName, path);
    } else if (op.equals("remove_folder")) {
      status = workspaces.removeFolder(workspaceName, path);
      if (!path.contains("/")) {
        gitBase.initGitRepositories();
      }
    } else if (op.equals("add_file")) {
      status = workspaces.addFile(workspaceName, path);
    } else if (op.equals("remove_file")) {
      status = workspaces.removeFile(workspaceName, path);
    }
    return status ? workspaces.getWorkspaceFileTree(workspaceName) : "";
  }

  @POST
  @Path("/write_file/{ws}/{path:.+}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_PLAIN)
  public String writeFile(@PathParam("ws") String workspaceName,
      @PathParam("path") String path, String content) {
    logger.info("write file for workspace " + workspaceName + " at " + path
        + " with content:\n" + content);
    return workspaces.writeFile(workspaceName, path, content);
  }
}