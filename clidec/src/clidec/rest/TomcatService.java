package clidec.rest;

import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import clidec.model.StandaloneTomcatServer;
import clidec.model.Workspaces;
import clidec.util.JsonUtil;
import clidec.util.MavenUtil;

import com.google.inject.Inject;

@Path("/tc")
public class TomcatService {

  @Inject
  private static Logger logger;

  @Inject
  private Workspaces workspaces;

  @Inject
  private StandaloneTomcatServer tomcat;

  @POST
  @Path("/{op}/{ws}/{path:.*}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public String deployToTomcatServer(@PathParam("op") String op,
      @PathParam("ws") String workspaceName, @PathParam("path") String path,
      Map<String, String> jsonParameters) {
    logger.info("request " + op + " web app to tomcat server at " + path);
    String projectName = workspaces.getProjectName(workspaceName, path);
    String projectPath = workspaces.getProjectPath(workspaceName, path);
    if (projectName == null || projectPath == null) {
      return "Invalide path " + workspaceName + "/" + path;
    }
    Map<String, String> parameters =
        JsonUtil.parseJsonParameters(jsonParameters);
    String message = "";
    if (op.equals("start")) {
      message = tomcat.start();
    } else if (op.equals("run")) {
      message += MavenUtil.executeGoals(projectPath, "war:war", null);
      String warFilePath =
          workspaces.findLatestWarFile(workspaceName, projectName);
      if (warFilePath == null) {
        message = "Failed to find latest war file for project " + projectName;
      } else {
        message += tomcat.run(projectName, warFilePath, parameters);
      }
    } else if (op.equals("deploy")) {
      if (!path.endsWith(".war")) {
        if (workspaces.isProjectFile(workspaceName, path)) {
          message += MavenUtil.executeGoals(projectPath, "war:war", null);
          String warFilePath =
              workspaces.findLatestWarFile(workspaceName, projectName);
          if (warFilePath == null) {
            message =
                "Failed to find latest war file for project " + projectName;
          } else {
            message += tomcat.deploy(projectName, warFilePath);
          }
        } else {
          message =
              "Can only deploy project or war file! Invalide path: " + path;
        }
      } else {
        String warFilePath =
            workspaces.getFileAbsolutePath(workspaceName, path);
        if (warFilePath == null) {
          message = "Invalide path " + workspaceName + "/" + path;
        } else {
          message = tomcat.deploy(projectName, warFilePath);
        }
      }
    } else if (op.equals("stop")) {
      message = tomcat.stop();
    } else {
      message = "Unsupport operation: " + op;
    }
    return message;
  }
}
