package clidec;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

import com.google.inject.Singleton;

@Singleton
public class EnvironmentConfig {

  private static Logger logger = Logger.getLogger(EnvironmentConfig.class
      .getName());

  public static final String PROPERTIES_FILE_NAME_PREFIX = "clidec";
  public static final String PROPERTIES_FILE_NAME_SUFFIX = "properties";
  public static final String USER_HOME_PROPERTY_NAME = "user.home";
  public static final String TOMECAT_HOME_PROPERTY_NAME = "CATALINA_HOME";
  public static final String MAVEN_HOME_PROPERTY_NAME = "M2_HOME";
  public static final String MAVEN_REPOSITORY_PROPERTY_NAME = "M2_REPOSITORY";

  private static final String MAC_OS_NAME_PREFIX = "Mac";
  private static final String WINDOWS_OS_NAME_PREFIX = "Windows";
  private static final String LINUX_OS_NAME_PREFIX = "Linux";
  private static final String DEFAULT_USER_HOME = "/home/clidec";
  private static final String DEFAULT_TOMCAT_HOME = "/var/lib/tomcat7";
  private static final String DEFAULT_MAVEN_HOME = "/home/clidec";

  private String userHome;
  private String tomcatHome;
  private String mavenHome;
  private String mavenRepository;

  public EnvironmentConfig() {
    try {
      String osName = System.getProperty("os.name");
      logger.info("os name: " + osName);
      String propertiesFileName = PROPERTIES_FILE_NAME_PREFIX;
      if (osName.startsWith(MAC_OS_NAME_PREFIX)) {
        propertiesFileName += ".mac";
      } else if (osName.startsWith(WINDOWS_OS_NAME_PREFIX)) {
        propertiesFileName += ".windows";
      } else if (osName.startsWith(LINUX_OS_NAME_PREFIX)) {
        propertiesFileName += ".linux";
      } else {
        logger.severe("unsupported os: " + osName);
        return;
      }
      propertiesFileName += "." + PROPERTIES_FILE_NAME_SUFFIX;
      Properties properties = new Properties();
      properties.load(Thread.currentThread().getContextClassLoader()
          .getResourceAsStream(propertiesFileName));
      userHome = properties.getProperty(USER_HOME_PROPERTY_NAME);
      tomcatHome = properties.getProperty(TOMECAT_HOME_PROPERTY_NAME);
      mavenHome = properties.getProperty(MAVEN_HOME_PROPERTY_NAME);
      mavenRepository = properties.getProperty(MAVEN_REPOSITORY_PROPERTY_NAME);

      if (userHome == null || userHome.equals("")) {
        userHome = System.getProperty(USER_HOME_PROPERTY_NAME);
        if (userHome == null || userHome.equals("")) {
          userHome = DEFAULT_USER_HOME;
        }
      }
      if (tomcatHome == null || tomcatHome.equals("")) {
        tomcatHome = System.getProperty(TOMECAT_HOME_PROPERTY_NAME);
        if (tomcatHome == null || tomcatHome.equals("")) {
          tomcatHome = DEFAULT_TOMCAT_HOME;
        }
      }
      if (mavenHome == null || mavenHome.equals("")) {
        mavenHome = System.getenv(MAVEN_HOME_PROPERTY_NAME);
        if (mavenHome == null || mavenHome.equals("")) {
          mavenHome = DEFAULT_MAVEN_HOME;
        }
      }
      if (mavenRepository == null || mavenRepository.equals("")) {
        mavenRepository =
            userHome + File.separator + ".m2" + File.separator + "repository";
      }
      logger.info("User home: " + userHome);
      logger.info("Tomcat home: " + tomcatHome);
      logger.info("Maven home: " + mavenHome);
      logger.info("Maven repository: " + mavenRepository);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String getUserHome() {
    return userHome;
  }

  public String getTomcatHome() {
    return tomcatHome;
  }

  public String getMavenHome() {
    return mavenHome;
  }

  public String getMavenRepository() {
    return mavenRepository;
  }

  public static void main(String[] args) {
    new EnvironmentConfig();
  }

}
