#!/bin/bash
# This file should be put into $TOMCAT_HOME/bin/

if [ -z "$1" ];then
    echo "Usage: $0 [port] [run|start|stop]"
    exit 1
fi
if [ -z "$2" ];then
    echo "Usage: $0 [port] [run|start|stop]"
    exit 1
fi
cd `dirname $0`
sed "s/8080/$1/g" < ../conf/server.xml > /tmp/server.xml \
    && nohup ./catalina.sh $2 -config /tmp/server.xml &